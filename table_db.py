#!/usr/bin/env python
# -*- encoding: utf-8 -*-

from bottle import get
from bottle import route
from bottle import post
from bottle import request
from bottle import Bottle
from bottle import run,static_file
from bottle import template

from bottle import response
from time import sleep

from db.db import DBconfig
from time import sleep


class Table():
    def __init__(self, tname="", tfields="", qfields="", theaders="", search=""):
        self.tfields = tfields
        self.tname = tname
        self.theaders = theaders
        self.qfields = qfields
        self.search = search
        self.order="" 
        self.url_query_table=""
        self.url_query_form=""
        self.db=None
        self._theaders=None
        self.modal="#generic-modal"
    
    def set_url_query_table(self,url):
        self.url_query_table=url
        
    def set_url_query_form(self,url):
        self.url_query_form=url
        
    def _where(self,phrase):
        return (self.search.format( phrase=phrase ) if phrase else "")
    
    def _order(self,request_method):
        self.order=""
        for key in request_method:
            for c in self.tfields:
                if "sort[%s]"%c == key:
                    print("ok")
                    self.order = "order by %s %s"%(c,request_method.get(key))
        return self.order
                    
    def __str__(self):
        if not self._theaders:
            self._theaders = [ (f,self.theaders[i]) for i,f in enumerate(self.tfields) ]            
        return template("templates/table.tpl",
                    tname=self.tname,
                    url_query_table=self.url_query_table,
                    url_query_form=self.url_query_form,
                    theader=self._theaders,
                    modal=self.modal
                    )  
    
    def is_table(self,name):
        return self.tname==name
    
    def db_query(self,where):
        if not self.db:
            self.db_connect()
        self.db.mkquery(self.qfields, self.tname, where, "")
        ret = self.db.fetchone()
        return ({ c:ret[i] for i,c in enumerate(self.tfields)})
        
    
    def db_get(self,pageNumber,rowCount,search,request_method):
        if not self.db:
            self.db_connect()
        where = self._where(search)
        order = self._order(request_method)
        total,current,res = self.db.query(pageNumber,rowCount,self.qfields ,self.tname,where,order)
        rows=[]
        for row in res: rows.append({ c:row[i] for i,c in enumerate(self.tfields)})
        return (total,current,rows) 
    
    def db_connect(self):
        self.db=DBconfig()
        self.db.connect('db/dbconfig.db')
        
    def db_close(self):
        self.db.close()
        self.db=None
        
        
    def db_execute(self,sql):
        if not self.db:
            self.db_connect()
        try:
            self.db.c.execute(sql)
            self.db.conn.commit()
        except:
            self.db.conn.rollback()

    
    
    
    
if __name__ == '__main__':     
    campos = ['id', 'usuario', 'password', 'permiso']
    campos = ['ID', 'Usuario', 'Contraseña', 'Permisos']
    tabla = 'usuarios'
    search="usuario like '%{phrase}%'"
    usuarios = Table(tabla,campos,campos,campos,search)

    print(usuarios.is_table("usuario"))

    a=usuarios
    print(a) 

    print(a._where("hola"))
    

