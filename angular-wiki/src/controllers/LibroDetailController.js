/**
 * LibroDetailController
 * Controlador de la ficha del libro
 * $scope - ámbito
 * $http - 
 * $routeParams - parámetros de la ruta
 */
function LibroDetailController($scope, $http, $routeParams) {
    $scope.id = $routeParams.libroId;

    $http.get('data/libro' + $scope.id + '.json').success(function (data) {
        $scope.libro = data[0];
    });

    $scope.editable=false;
    $scope.editar = function () {
        $scope.editable=true;
    };
    $scope.guardar = function () {
        $scope.editable=true; 
    };
    $scope.cancelar= function () {
        $scope.editable=false;
    };


    $scope.selected = undefined;
    $scope.states = ['Alabama', 'Alaska', 'Arizona', 'Arkansas', 'California', 
            'Colorado', 'Connecticut', 'Delaware'];
}