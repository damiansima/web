var app = angular.module('appAng', ['ngRoute','textAngular','ngSanitize','ui.bootstrap']);

//definimos las rutas de la 'app'
app.config(function ($routeProvider) {

    $routeProvider.
            when('/', {
                templateUrl: 'pages/home.html',
                controller: 'mainController'
            }).
            when('/libros', {
                templateUrl: 'src/views/libros-list.html',
                controller: LibrosListController
            }).
            //mediante dos puntos (:) definimos un parámetro
            when('/libro/:libroId', {
                templateUrl: 'src/views/libro.html',
                controller: LibroDetailController
            }).
            //cualquier ruta no definida  
            otherwise({
                redirectTo: '/libros'});

});

app.controller('mainController', function ($scope) {
    $scope.message = 'Hola, Mundo!';
});


