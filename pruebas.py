from bottle import get
from bottle import post
from bottle import request
from bottle import Bottle
from bottle import run,static_file
from bottle import template

from bottle import response

app = Bottle()

#@app.hook('after_request')
def enable_cors():
    """
    You need to add some headers to each request.
    Don't use the wildcard '*' for Access-Control-Allow-Origin in production.
    """
    response.headers['Access-Control-Allow-Origin'] = '*'
    response.headers['Access-Control-Allow-Methods'] = 'PUT, GET, POST, DELETE, OPTIONS'
    response.headers['Access-Control-Allow-Headers'] = 'Origin, Accept, Content-Type, X-Requested-With, X-CSRF-Token'
    print("por aca")


html= """
<!doctype html>
<html>
 
    <head>
        <title>Hello</title>
        
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
         
        <script type="text/javascript">
            var updater = {
                poll: function() {
                    jQuery.ajax({
                        type: "POST",
                        url: "/ajax",
                        data: {textfield : 'hola' },
                        success: function(data) {
                            var output = "";            
                            for (i in data) {
                                jQuery("#text."+i).text(data[i])
                                jQuery("#bar ."+i).css("width", Math.floor(3*data[i])+"px");
                            }
                            window.setTimeout(updater.poll, 4000);
                        },
                    });
                },
            }
            
            jQuery(document).ready(function() {
            jQuery(".button2").click(function() {
                alert("hola")
                })
            
            jQuery(".button").click(function() {
                var input_string = $("input.text").val();
                var ii=''
                jQuery.ajax({
                    type: "POST",
                    url: "/ajax",
                    data: {textfield : input_string },
                    success: function(data) {
                            var output = "";            
                            for (i in data) {
                                jQuery("#"+i).text(data[i])
                            }
                        },
                    });
                    return false;
                });
                // alert("alerta")
                updater.poll();
             });
             
        </script>
    </head>
     
    <body>
        <br>
        <form class="form" method="post"> 
        <input class="text" type="text" value="hola"/>    
        <input class="button" type="submit" value="send"/>    
        </form>
 
        <br><br>
        <div id="bar" style="height:30px; background: #000; width:300px">
        <div class="Variable_1" style="height:30px; background: #0a0;display: block;"></div>
        </div>
        <p><span id="text" class="Variable_1"</span></p>        
    </body>
     
</html>
"""

 
@app.route('/static/<path:path>/:filename')
def server_static1(path,filename):
    print(' path static: ', path,filename) 
    return static_file(filename, root='static/'+path)
    
@app.route('/static/:filename')
def server_static2(filename):
    return static_file(filename, root='static/')


@app.route('/yea2', method="POST")
def yea2p():
    desde = request.forms.get('desde')
    hasta = request.forms.get('hasta')
    # print('input',request.POST['nsend'])

    print('desde hasta')
    print(desde,hasta)
    print(str2time(desde),str2time(hasta))

    print (TAGS)
    return html2

@app.route('/total-count/')
def total_count():
    x= {"html": 123 }
    print(x)
    return x

@app.route('/')
def index():
    return template('index.html', user='')
    
# @app.get('/updateData')
# def login_form():
    # return '''<form method="POST" action="/updateData">
                # <input name="name"     type="text" />
                # <input type="submit" />
              # </form>'''

@app.route('/task_add', method="POST") 
def submit_form2():
    print("ajax command add") 
    return {"data":"yea","data2":"nada"}

@app.route('/ajax', method="POST")
def submit_form():
    print("ajax command add") 
    for x in request.forms:
        print(x)
    return {"data":"yea","data2":"nada"}


@app.route('/tablas/<usuario>/<name>')
def tasks_add(usuario,name): 
    msg="<strong>tabla: %s campo: %s</strong><br>"%(usuario,name)
    for x in request.GET:
        print(x)
        msg +="<strong>%s</strong>:%s<br>"%(x,request.GET.getall(x))
    msg_info="""
<div class="alert alert-block alert-success">
    <button type="button" class="close" data-dismiss="alert">
      <span aria-hidden="true">&times;</span>
      <span class="sr-only">Close</span>
    </button>
    %s
</div>"""%(msg)
    return  {   "inner-fragments": 
                    {"#msg-alert2":msg},
                "append-fragments": 
                    {".msg_info":msg_info}
            } 



@app.route('/json/tablas/x', method="POST") 
def json_tablas():
    return {
        "current": -1,
        "rowCount": -1,
        "rows": [
            {
            "id": 19,
            "sender": "123@test.de",
            "received": "2014-05-30T22:15:00"
            },
            {
            "id": 14,
            "sender": "123@test.de",
            "received": "2014-05-30T20:15:00"
            }
        ],
        "total": 2
        }





@app.route('/<base>/<filepath:path>')
def server_static(base,filepath):
    #print("base:",base)
    #print("filepath:",filepath)
    return static_file(filepath, root=base)


run(app, host='0.0.0.0', port=8000)
