<!--<!DOCTYPE html>-->
<table class="table table-condensed table-hover" name="{{tname}}">
<thead>
    <tr>
    % for i,v in theader: 
        % if "edit" == i:
            <th  data-column-id="edit" data-sortable="false" data-formatter="edit">Editar</th>
        % else:
            <th data-column-id="{{i}}">{{v}}</th>
        % end 
    % end 
    </tr>
</thead>
</table>

<script>
    $("#table-edit table").bootgrid("destroy");
    $("#table-edit table").bootgrid({
        rowCount: 10,
        ajax: true,
        url: "{{url_query_table}}",
        formatters: {
            edit: function (column, row){
                return '<a id="command-edit" class="btn btn-xs" data-form-url="{{url_query_form}}" data-row-edit="' +row.edit+ '" data-target="#modal-mensajes"><span class=\"fa fa-fw fa-pencil\"></span> </a>';
                
            }
        }
    }).on("loaded.rs.jquery.bootgrid", function(){
        $("table a#command-edit").on("click", function(e){
            modal=$("{{modal}}");
            reload=modal.find("a.reload");
            url_qf = "{{url_query_form}}/" + $(this).data("row-edit");
            reload.attr("href",url_qf);  
            
            modal_body = modal.find(".modal-body");
            modal.find(".modal-title").text("Cargando...");
            modal_body.html("<p class='text-center'><i class='fa fa-fw fa-spinner fa-spin fa-3x'></i></p>")
            modal.modal('show');
            
            reload.trigger("click");  
            
        })  
    });
</script>
