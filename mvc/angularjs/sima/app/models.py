from app import app,db


import os
import os.path

from flask.ext.restless import APIManager
# from flask.ext.sandboy import Sandboy
from flask.ext.sqlalchemy import SQLAlchemy


class Person(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.Unicode)


class RFID(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    tag = db.Column(db.String(255), unique=True)
    
    def __init__(self, tag):
        self.tag = tag
    
    
class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(255), unique=True)
    Name = db.Column(db.String(255))
    FirstName = db.Column(db.String(255))
    password = db.Column(db.String(255))
    active = db.Column(db.Boolean())
    confirmed_at = db.Column(db.DateTime())
    
    def __init__(self, email):
        self.email = email
        
    def __repr__(self):
        return '<User %r>' % (self.email) 

#db.create_all()
#for i in range(1, 10):
    #person = Person(name=u'person' + str(i))
    #db.session.add(person)
#db.session.commit()

#print (Person.query.all())

def check_auth(*instance_id, **kw):
    print("instance_id",instance_id)  
    print("kw",kw)
    
    
    
api_manager = APIManager(app, flask_sqlalchemy_db=db, preprocessors=dict(GET=[check_auth])) 
api_manager.create_api(Person, methods=['GET', 'POST', 'DELETE'])  
api_manager.create_api(RFID, methods=['GET', 'POST', 'DELETE'])  
api_manager.create_api(User, methods=['GET', 'POST', 'DELETE'])  

#sandboy = Sandboy(app, db, [Person, RFID]) 



