angular.module('libros', []).

  //definimos las rutas de la 'app'
  config(['$routeProvider', function($routes) {
	  
  $routes.
      when('/libros', {
		  templateUrl: 'src/views/libros-list.html',
		  controller: LibrosListController
		  }).
	  
	  //mediante dos puntos (:) definimos un parámetro
      when('/libro/:libroId', {
		  templateUrl: 'src/views/libro.html',
		  controller: LibroDetailController
		  }).
	 
	  //cualquier ruta no definida  
      otherwise({
		  redirectTo: '/libros'});

}]).controller('MyController', ['$scope', function ($scope) {
	$scope.test = 'World';
	
	  //esta función es mi controlador
	$scope.total = 0;
	$scope.cuanto = 0;

	$scope.sumar = function(){
		$scope.total += parseInt($scope.cuanto);
	}
	$scope.restar = function(){
		$scope.total -= parseInt($scope.cuanto);
	}
	
}]);



