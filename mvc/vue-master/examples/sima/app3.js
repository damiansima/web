var validator = window['vue-validator']
Vue.use(validator)

Vue.filter('JSON', function (value) {
    return JSON.stringify(value, null, "\t");
});
    
Vue.component('from-group', {
    template: '<div class="form-group has-feedback"  v-class="\
                has-success : valid.dirty && valid.valid,\
                has-error : valid.dirty && valid.invalid ">\
                <label class="col-sm-{{width}} control-label" >{{title}}</label>\
                <div class="col-sm-{{12-width}}" data-toggle="tooltip" data-placement="top" title="{{tooltip}}">\
                    <content></content> \
                    <span v-if="valid.dirty" class="glyphicon form-control-feedback" aria-hidden="true"\
                          v-class="glyphicon-ok: valid.valid, glyphicon-remove: valid.invalid" \
                    ></span>\
                </div></div> ',
            
    replace: true,
    paramAttributes: ['width','valid','title'],
    data: function () {return {
        valid:false,
        tooltip: 'causa',
    }},
    created : function () {
        this.$watch('valid.valid', function () {
            this.tooltip="Para validar se requiere:  \n" ;
            if(this.valid.valid) this.tooltip = " Validado\n";
            if(this.valid.numeric) this.tooltip += " - numerico\n";
            if(this.valid.integer) this.tooltip += " - entero\n";
            if(this.valid.min) this.tooltip += " - valor mayor\n";
            else if(this.valid.max) this.tooltip += " - valor menor\n";
            if(this.valid.alpha) this.tooltip += " - alfabetico\n";
            if(this.valid.alphaNum) this.tooltip += " - alfanumerico\n";
            if(this.valid.minLength) this.tooltip += " - longitud mayor\n";
            else if(this.valid.maxLength) this.tooltip += " - longitud menor\n";
            if(this.valid.length) this.tooltip += " - longitud exacta\n";
            console.log(this.tooltip); 
            console.log(this.valid ); 
            console.log(this.valid.toString() );  
        });  
    }, 
})

Vue.component('from-submit', {
    template: '<div class="form-group">\
                 <div class="col-sm-offset-{{width}} col-sm-{{12-width}}">\
                   <content></content>\
                   <button type="submit" class="btn btn-primary" v-class="disabled : !valid ">Submit</button>\
                 </div>\
               </div>',
            
    replace: true,
    paramAttributes: ['width','valid'],
})

Vue.partial('avatar2', '<span v-if="validation[{{avatar_test}}].dirty" class="glyphicon form-control-feedback" aria-hidden="true"\
                          v-class="glyphicon-ok: validation[{{avatar_test}}].valid, glyphicon-remove: validation[{{avatar_test}}].invalid" \
                    ></span>')
Vue.partial('avatar', '<p>Partial1: {{texto}} </p>')

Vue.component('from-group-bkp', {
    template: '<div class="form-group has-feedback"  v-class="\
                has-success : valid.dirty && valid.valid,\
                has-error : valid.dirty && valid.invalid ">\
                <label class="col-sm-{{width}} control-label" >{{title}}</label>\
                <div class="col-sm-{{12-width}}" data-toggle="tooltip" data-placement="top" title="{{tooltip}}">\
                    <content></content> \
                    <span v-if="valid.dirty" class="glyphicon form-control-feedback" aria-hidden="true"\
                          v-class="glyphicon-ok: valid.valid, glyphicon-remove: valid.invalid" \
                    ></span>\
                </div></div> ',
            
    replace: true,
    paramAttributes: ['width','valid','title'],
    data: function () {return {
        valid:false,
        tooltip: 'causa',
    }},
    created : function (el) {
        this.$watch('valid.valid', function () {
            this.tooltip="Para validar se requiere:  \n" ;
            if(this.valid.valid) this.tooltip = " Validado\n";
            if(this.valid.numeric) this.tooltip += " - numerico\n";
            if(this.valid.integer) this.tooltip += " - entero\n";
            if(this.valid.min) this.tooltip += " - valor mayor\n";
            else if(this.valid.max) this.tooltip += " - valor menor\n";
            if(this.valid.alpha) this.tooltip += " - alfabetico\n";
            if(this.valid.alphaNum) this.tooltip += " - alfanumerico\n";
            if(this.valid.minLength) this.tooltip += " - longitud mayor\n";
            else if(this.valid.maxLength) this.tooltip += " - longitud menor\n";
            if(this.valid.length) this.tooltip += " - longitud exacta\n";
//            console.log(this.tooltip); 
//            console.log(this.$parent.validation['form.password']); 
            
        });  
        console.log(this.$parent); 
        console.log(this.$parent.$el); 
        console.log(this.$parent.$data.form); 
        console.log(this.$parent.form); 
        console.log(this.$parent.validation); 
        console.log(this.$parent.validation['form.password']); 
        
//        this.val = this.$parent.validation; 
//        console.log(this.val); 
//        this.$watch("val['form.password']", function () {
//            console.log("--aca:"+this.val); 
//        });
        
    }, 
})

Vue.component('my-input', {
    template: '<spam v-if="texto">PERFECTO</spam>',
    paramAttributes: ['texto'], 
            
    methods: {
        test: function (e) {
            var model = this.$el.$.getAttribute('texto'); 
            console.log('texto my-components:'+model);
            console.log(this.texto);
        }
    },
})

var demo = new Vue({
    el: '#validator-forms',
    data: {
        avatar_test: '"form.number"',
        avatarURL: '/images/avatar.jpg',
        texto:  'Hello Vue.js!',
        form: {
            password: '',
        },
        validation: {},
    },
    methods: {
        test: function (e) {
            console.log(this.texto);
        }
    },
})
