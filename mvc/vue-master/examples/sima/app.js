define(function (require) {

    var Vue = require('./lib/vue');
    var validator = require('./lib/validator');
    Vue.use(validator);

    Vue.config.delimiters = ['{{', '}}']
    Vue.config.debug = true

    Vue.component('user-profile', {
        template: '{{ancho}}-{{dbfield}}-{{dbname}}',
    })

    Vue.component('input-text', {
        template: '#input-text'
    })

    Vue.component('x-component', {
        template: '<input type="text" v-model="name" v-valid="required,minLength:3,alpha">',
        compiled: function () {
            this.$watch('name', function (value) {
                this.$parent.name = value; 
                console.log(value); 
            })
        }
    })
    
    Vue.filter('JSON', function (value) {
        return JSON.stringify(value, null, "\t");
    });

    Vue.component('my-element', {
        template: '<h1>This is a Vue custom element!</h1><h2>{{msg}}</h2><content></content>',
        paramAttributes: ['msg']
    })

    var demo = new Vue({
        el: '#ex_tags',
        components: {
            test: {
                template:  '<div class="form-group has-feedback"  v-class="\
                            has-success : validator.form.prueba.valid,\
                            has-error : validator.form.prueba.invalid">\
                            <label class="col-sm-4 control-label">Prueba X</label>\
                            <div class="col-sm-8">\
                                <input type="text" class="form-control" v-model="form.prueba" model="form.prueba"  v-valid="required,alpha"/> \
                                <span class="glyphicon form-control-feedback" aria-hidden="true"\
                                      v-class="glyphicon-ok: validator.form.prueba.valid, glyphicon-remove: validator.form.prueba.invalid"></span>\
                            </div></div>',
//                compiled: function () {
//                    this.$watch('text', function (value) {
//                        this.$parent.form.test = value; 
//                    })
//                }
            }
        },
        data: {
            valido: true,
            validator: {},
            from:{
                test: ''
            },
            tags: {
                Temperatura: 3,
                Tension: 334.34
            },
            user: {
                name: 'Foo Bar',
                email: 'foo@bar.com'
            },
            config: {
                nombre: {
                    ancho: 2,
                    dbfield: 'nombre',
                    dbname: 'Nombre'
                },
                prueba: {
                    ancho: 2,
                    dbfield: 'prueba',
                    dbname: 'Alta Prueba'
                }
            },
            message: 'Hello Vue.js!'
        },
        methods: {
            ontest: function () {
                this.message = "nada"
            }
        },
        //     compiled: function () {
        //        var self = this
        //        $.ajax({
        //            success: function (data) {
        //            self.$data = data
        //            self.$emit('data-loaded')
        //            }
        //        })
        //    }
    });
});



//function eajax(url, method, data,func) {
//        var newdata = false;
//        $.ajax({
//            url: url,
//            type: method,
//            contentType: "application/json",
//            data: data,
//            success: function(data) {
//                    func(data);
//                },
//            error: function (xhr, ajaxOptions, thrownError) {
//                    var msg = jQuery.parseJSON(xhr.responseText);
//                    // alert(String.format("Error {0}: {1} \nMensage: {2}",xhr.status,thrownError,msg.message));
//                    add_message('danger',xhr.status,thrownError+" "+msg.message); 
//                }
//            }); 
//        };
//        
//    function func1(data) {
//        alert(JSON.stringify(data));
//    };
//    
//    function getform(form){
//        var data={},datas=form.serializeArray();
//        for(var i in datas){
//            data[datas[i].name]=datas[i].value;
//        };
//        return JSON.stringify(data);
//    };
//    
//    
//    $( "form" ).submit(function( event ) {
//        event.preventDefault();
//        var data = getform( $(this) ); 
//        eajax( '/api/person','POST',data, func1 );  
//    });

