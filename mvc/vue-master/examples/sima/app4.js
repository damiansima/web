var validator = window['vue-validator']
Vue.use(validator)

Vue.filter('JSON', function (value) {
    return JSON.stringify(value, null, "\t");
});
    
Vue.component('my-element2', {
    template: '<content></content>',
    paramAttributes: ['msg'],
    replace: true,
    data: function () {return {
        msg: "null",
        html: '<h1>texto html</h1>',
    }},
    compiled : function () {
        console.log(this.msg);
//            this.html= '<h1>NADA texto html (('+this.msg+'))</h1>';
        this.html= '<form class="form-horizontal" >\
        <div class="form-group has-feedback"  v-class="\
            has-success : validator.form.dbfield.valid,\
            has-error : validator.form.dbfield.invalid\
            ">\
            <label class="col-sm-4 control-label">Nombre</label>\
            <div class="col-sm-8">\
                <input type="text" class="form-control" v-model="form.dbfield" v-valid="required,alpha"/> \
                <span class="glyphicon form-control-feedback" aria-hidden="true"\
                      v-class="glyphicon-ok: validator.form.dbfield.valid, glyphicon-remove: validator.form.dbfield.invalid"></span>\
            </div></div><br>'+this.msg+'<content></content><br><a href="#a" v-on="click: test">click</a> ';
        this.$el.innerHTML = this.html;
//            this.$watch('content', function () {
//                this.$el.value = JSON.stringify(this.content, null, 2);
//            });  
    }, 
    methods: {
        test: function (e) {
            console.log(this.content);
            var self = this;
            console.log(JSON.stringify(self.validator, null, 2)); 
        }
    }
})

Vue.component('my-element', {
//        template: '<br>This is a Vue custom element! <br>msg:{{msg}}<br><content></content><br>',
    data: function () {return {
        msg: "null",
        validator:'nada',
    }},
    template: '<form class="form-horizontal" >\
        <div class="form-group has-feedback"  v-class="\
            has-success : validator.form.dbfield.valid,\
            has-error : validator.form.dbfield.invalid\
            ">\
            <label class="col-sm-4 control-label">Nombre</label>\
            <div class="col-sm-8">\
                <input type="text" class="form-control" v-model="form.dbfield" v-valid="required,alpha"/> \
                <span class="glyphicon form-control-feedback" aria-hidden="true"\
                      v-class="glyphicon-ok: validator.valid, glyphicon-remove: validator.form.dbfield.invalid"></span>\
            </div></div><br>'+this.msg+'<content></content><br>',
    paramAttributes: ['msg','dbfield'],
//        replace: true,

    methods: {
        test: function (e) {
            console.log(this.content);
            var self = this;
            console.log(JSON.stringify(self.validator, null, 2)); 
        }
    }
});

Vue.component('from-group', {
    template: '<div class="form-group has-feedback"  v-class="\
                has-success : valid.dirty && valid.valid,\
                has-error : valid.dirty && valid.invalid ">\
                <label class="col-sm-{{width}} control-label" >{{title}}</label>\
                <div class="col-sm-{{12-width}}" data-toggle="tooltip" data-placement="top" title="{{tooltip}}">\
                    <content></content> \
                    <span v-if="valid.dirty" class="glyphicon form-control-feedback" aria-hidden="true"\
                          v-class="glyphicon-ok: valid.valid, glyphicon-remove: valid.invalid" \
                    ></span>\
                </div></div> ',
            
    replace: true,
    paramAttributes: ['width','valid','title'],
    data: function () {return {
        valid:false,
        tooltip: 'causa',
    }},
    created : function () {
        this.$watch('valid.valid', function () {
            this.tooltip="Para validar se requiere:  \n" ;
            if(this.valid.valid) this.tooltip = " Validado\n";
            if(this.valid.numeric) this.tooltip += " - numerico\n";
            if(this.valid.integer) this.tooltip += " - entero\n";
            if(this.valid.min) this.tooltip += " - valor mayor\n";
            else if(this.valid.max) this.tooltip += " - valor menor\n";
            if(this.valid.alpha) this.tooltip += " - alfabetico\n";
            if(this.valid.alphaNum) this.tooltip += " - alfanumerico\n";
            if(this.valid.minLength) this.tooltip += " - longitud mayor\n";
            else if(this.valid.maxLength) this.tooltip += " - longitud menor\n";
            if(this.valid.length) this.tooltip += " - longitud exacta\n";
            console.log(this.tooltip); 
            console.log(this.valid ); 
            console.log(this.valid.toString() );  
        });    
//        this.valido=this.$parent.validation;
////        console.log(this.valid); 
//        this.$watch('valido.valid', function () {
//            console.log(this.valid); 
//        });
    }, 
})

Vue.component('my-input', {
    template: '<spam v-if="texto">PERFECTO</spam>',
    paramAttributes: ['texto'], 
            
    methods: {
        test: function (e) {
            var model = this.$el.$.getAttribute('texto'); 
            console.log('texto my-components:'+model);
            console.log(this.texto);
        }
    },
})

var demo = new Vue({
    el: '#editor',
    data: {
        texto:  'Hello Vue.js!',
    },
    methods: {
        test: function (e) {
            console.log(this.texto);
        }
    },
})
