define(function (require) {

    var Vue = require('./lib/vue');
    var validator = require('./lib/validator'); 
    Vue.use(validator); 

    Vue.component('schema-links', {
        data: {
            links: {
                "update": "nada",
            }
        },
        methods: {
            setHref: function (e) {
                e.preventDefault();
                console.log(e.target);

                var self = this;
                self.$parent.doc = {};
                self.$parent.schema =  { "title": "Example Schema",
                    "type": "object",
                    "properties": {
                        "firstName": {
                            "type": "text"
                        },
                        "lastName": {
                            "type": "text"
                        },
                        "age": {
                            "description": "Age in years",
                            "type": "number",
                            "minimum": 0
                        }
                    },
                    "required": [
                        "firstName",
                        "lastName"
                    ]
                };
                
            }
        }
    });

    Vue.component('vue-schema', {
        filters: {
            input_type: function (value) {
                var types = {
                    string: 'text',
                    integer: 'number'
                }
                return types[value];
            }
        }
    });

    Vue.component('my-element', {
//        template: '<br>This is a Vue custom element! <br>msg:{{msg}}<br><content></content><br>',
        template: '<form class="form-horizontal" >\
            <div class="form-group has-feedback"  v-class="\
                has-success : validator.form.dbfield.valid,\
                has-error : validator.form.dbfield.invalid\
                ">\
                <label class="col-sm-4 control-label">Nombre</label>\
                <div class="col-sm-8">\
                    <input type="text" class="form-control" v-model="form.dbfield" v-valid="required,alpha"/> \
                    <span class="glyphicon form-control-feedback" aria-hidden="true"\
                          v-class="glyphicon-ok: validator.form.dbfield.valid, glyphicon-remove: validator.form.dbfield.invalid"></span>\
                </div></div><br>'+this.msg+'<content></content><br>',
        paramAttributes: ['msg','dbfield'],
//        replace: true,
        data: function () {return {
            msg: "null",
        }},
        methods: {
            test: function (e) {
                console.log(this.content);
                var self = this;
                console.log(JSON.stringify(self.validator, null, 2)); 
            }
        }
    });
    
    
    Vue.component('json-textarea', {
        template: '{{content}}',
        paramAttributes: ['schema'],
        
        created: function () {
            this.$watch('content', function () {
                this.$el.value = JSON.stringify(this.content, null, 2);
            });  
        }, 
        methods: {
            updateSchema: function (e) {
//                console.log(this.content);
                var self = this;
                console.log(self.schema); 
//                console.log(self.$parent.$msg);
//                self.$parent.schema.title = "Pepe";
//                console.log(self.$parent.$data['schema']);
                self.$parent[self.schema] = JSON.parse(e.target.value);
//                e.targetVM.content = JSON.parse(e.target.value); 
            }
        }
    });




    Vue.component('json-schema-property', {
        template: '#json-schema-property'
    });

    var app = new Vue({
        el: '#editor',
        data: {
            schema :  { "title": "Example Schema",
                    "type": "object",
                    "properties": {
                        "firstName": {
                            "type": "text"
                        },
                        "lastName": {
                            "type": "text"
                        },
                        "age": {
                            "description": "Age in years",
                            "type": "number",
                            "minimum": 0
                        }
                    },
                    "required": [
                        "firstName",
                        "lastName"
                    ]
                }
        },
        methods: {
            output: function () {
                var jsonDOM = this.$el.querySelectorAll('[data-json]');
                var json = {};
                function accumulate(obj, dom) {
                    for (var i = 0; i < dom.length; i++) {
                        if (dom[i].dataset['json'] == 'kvp') {
                            obj[dom[i].querySelector('label').textContent] = dom[i].querySelector('input').value;
                        } else if (dom[i].dataset['json'] == 'object') {
                            var legend = dom[i].querySelector('legend').textContent;
                            var sub_dom = dom[i].querySelectorAll('[data-json]');
                            obj[legend] = accumulate({}, sub_dom);
                            i += sub_dom.length;
                        }
                    }
                    return obj;
                }
                return document.querySelector('#output').value = JSON.stringify(accumulate(json, jsonDOM), null, "\t");
            },
            test: function (e) {
                console.log(this.content);
                var self = this;
                console.log(JSON.stringify(self.validator, null, 2)); 
            }
        }
    });

});