from bottle import get
from bottle import post
from bottle import request
from bottle import Bottle
from bottle import run,static_file
from bottle import template

from bottle import response




modal = """
<div class="modal fade" id="{{ config["id"] or None }}" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="panel panel-{{config["mode"]}}">
            <div class="panel-heading">
            	% if config["button-close"]:
	    		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	    		% end
                <h3 class="panel-title" id="contactLabel"><span class="{{config["title-icon"]}}"></span> {{config["title"]}} </h3>
            </div>
            <form class="ajax" action="/tablas/usuarios/send" method="get" data-reload-url="/tablas/usuarios/" accept-charset="utf-8">
                <div class="modal-body onchange" name="allforms" style="padding: 5px;">
                  <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 10px;">
                            <select name="combo" class="selectpicker show-menu-arrow " data-width="100%" data-live-search="true" data-size="5">
                                <option>Mustard</option>
                                <option>Ketchup</option>
                                <option>Relish</option>
                                <option>Tent</option>
                                <option>Flashlight</option>
                                <option>Toilet Paper</option>
                                <option>Tent</option>
                                <option>Flashlight</option>
                                <option>Toilet Paper</option>
                            </select>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 10px;">
                            <input class="form-control onffocus" name="email" placeholder="E-mail" type="text" required />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12" style="padding-bottom: 10px;">
                            <input class="form-control" name="subject" placeholder="Subject" type="text" required />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <textarea style="resize:vertical;" class="form-control" placeholder="Message..." rows="6" name="comment" required></textarea>
                        </div>
                    </div>
                </div>  
                <div class="panel-footer" style="margin-bottom:-14px;">
                    <input type="submit" class="btn btn-success" value="Send"/>
                        <!--<span class="glyphicon glyphicon-ok"></span>-->
                    <input type="reset" class="btn btn-danger" value="Clear" />
                        <!--<span class="glyphicon glyphicon-remove"></span>-->
                    <button style="float: right;" type="button" class="btn btn-default btn-close" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div> 
"""

config={
	"id" : "modal-contact" ,
	"mode" : "primary",
	"button-close": True ,
	"title" : "Hola mundo",
	"title-icon" : "fa fa-camera-retro fa-4x"
}

print("configuracion:",config)

ret=template( modal, user='', config=config )
print(ret)