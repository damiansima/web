#!/usr/bin/env python
# -*- encoding: utf-8 -*-
### -*- encoding: utf-8 -*-

from bottle import get
from bottle import post
from bottle import request
from bottle import Bottle
from bottle import run,static_file
from bottle import template

from bottle import response
from db.db import DBconfig
from time import sleep

from table_db import Table

app = Bottle()


def f_button(type="", mode="default", text=None, icon="",param="" ):
    if type=="submit":
        icon="fa fa-fw fa-download"
        if not text: text = "Guardar" 
    if type=="reset":
        icon="fa fa-fw fa-refresh"
        if not text: text = "Limpiar"
    if type=="close":
        type="button"
        icon="fa fa-fw fa-times"
        mode+=" btn-close"
        param='style="float: right;" data-dismiss="modal"'
        if not text: text = "Cerrar"
    return """ <button type="{type}" class="btn btn-{mode}" {param}><span class="{icon}"> </span> {text}</button> """.format(
            type=type,mode=mode,icon=icon,param=param,text=text)
                

def f_row(content):
    return """<div class="row">{content}</div>\n""".format(content=content)
            
def f_col(content,width_small=False,width_medium=False,width_long=False,offset=False):
    width=""
    if width_small : width+="col-sm-%s "%width_small
    if width_medium : width+="col-md-%s "%width_medium
    if width_long : width+="col-lg-%s "%width_long
    return """<div class="{width}">{content}</div>\n""".format(content=content,width=width)     

    
def f_lablel(text="...", width=2):
    return """<label class="control-label col-xs-{width}">{text}</label>""".format(text=text,width=width)


    
def f_input(name="name",text="...",id="", type="text", param="", classarg="", required=False ):
    if required: 
        param+=" required"
    return """<input id="{id}" class="form-control {classarg}" name="{name}" placeholder="{text}" type="{type}" {param} />
        """.format(name=name,id=id,text=text,type=type,param=param,classarg=classarg)


def f_select(name="name",options=[1,2],id="", param="", classarg="", size=5, search=False ):
    if search: 
        param+=' data-live-search="true"'
    options = "\n".join([ "<option>%s</option>"%i for i in options ]) 
    return """<select name="{name}" id="{id}" class="selectpicker show-menu-arrow {classarg}" {param} data-width="100%"  data-size="{size}">
        {options}</select>""".format(name=name,id=id,size=size,param=param,classarg=classarg,options=options)
        
    
sel = f_select(name="select1",options=["sima","yea men"],search=True)
    
print(f_button(type="submit"))
footer=f_button(type="submit",mode="primary")+f_button(mode="primary",type="reset")
#footer = f_button(mode="primary",text="",icon="fa fa-times", param='data-dismiss="modal"' ) 

button=f_button( mode="primary",text="modal",icon="glyphicon glyphicon-edit", param='data-toggle="modal" data-target="#contact"' ) 
test = f_col(button,9,5,2) + f_col(button,2,2,2)  + f_col(button,2,2,2)
button=f_button( mode="info",text="modal",icon="glyphicon glyphicon-edit", param='data-toggle="modal" data-target="#contact"' ) 
input = f_input(name="name",text="contrasena",type="password", required=True )
test2 = f_col(button,2,5,9) + f_col(sel,2,2,2) + f_col(input,2,5,2) 
button = f_row(test) + f_row(test2)
print(button)
        

    
p_admin="""
    
    <h1>Administracion</h1>

    <a class="btn btn-info" data-toggle="modal" data-target="#cargar-db" data-original-title>
        Mensajes 
    </a>

    <div class="modal fade" id="cargar-db" tabindex="-1" role="dialog" aria-labelledby="contactLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <a type="button" class="close" data-dismiss="modal" > <span class="fa fa-fw fa-times"> </span>  </a>
                    <a type="button" class="ajax close reload" href="cargar-db" > <span class="fa fa-fw fa-refresh"> </span>  </a>
                    <h4 class="modal-title">Titulo</h4>
                </div>
                <div class="modal-body" > 
                    asd
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).on('eldarion-ajax:complete', function(){
            $('.selectpicker').selectpicker('refresh'); 
        });
    </script>
"""

html="""

 <div class="modal fade" id="generic-modal" tabindex="-1" role="dialog" aria-labelledby="contactLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div id="genEditModal" class="modal-content">
            <div class="modal-header">
                <a type="button" class="close" data-dismiss="modal" > <span class="fa fa-fw fa-times"> </span>  </a>
                <a type="button" class="ajax close reload" href="" > <span class="fa fa-fw fa-refresh"> </span>  </a>
                <h4 class="modal-title">Titulo</h4>
            </div>
            <div class="modal-body" > 
                asd
            </div>
        </div>
    </div>
    <script>
        $(document).on('eldarion-ajax:complete', function(){
            $('.selectpicker').selectpicker('refresh'); 
        });
    </script>
</div>

<div class ="well" >
    <form class="form-horizontal" method="get" data-reload-url="/ajax/cargatabla/" >
        <div class="onchange">
            <select name="tname" class="selectpicker" multiple title='Elija la tabla' data-max-options="1">
                <option>rfid</option>
                <option>usuarios</option>
            </select>
        </div>
    </form>
     
    <script>
        $("form .onchange").change( function() {
            $(document).trigger('ajax:reload', [$(this), ""]); 
        });
    </script>


</div>

<div id="table-edit">
</div>


<script>
$("#activselect").click( function() {
        $('.selectpicker').selectpicker('refresh');
        $("#table-edit table").bootgrid({rowCount:5});
    });
    
$("#eliminatabla").click( function() {
        $("#table-edit table").bootgrid("destroy"); 
    });
    
</script>
"""

#.format(picker=f_select(name="select1",options=["sima","yea men"],search=True))

table_ajax="""
<div id="table-edit" class="">
    <table class="table table-condensed table-hover" name="{{tname}}">
    <thead>
        <tr>
        % for i,v in theader: 
            <th data-column-id="{{i}}">{{v}}</th>
        % end
        % if editable:
            <th  data-column-id="edit" data-sortable="false" data-formatter="edit">Editar</th>
        % end
        </tr>
    </thead>
    </table>
    
    <script>
        $("#table-edit table").bootgrid("destroy");
        $("#table-edit table").bootgrid({
            rowCount: 3,
            ajax: true,
            url: "/ajax/query/{{tquery}}",
            formatters: {
                edit: function (column, row){
                    return '<a id="command-edit" class="btn btn-xs" data-form-url="/ajax/form/{{tname}}/'+row.id+'" data-target="#{{modalid}}"><span class=\"fa fa-fw fa-pencil\"></span> </a>';
                    
                }
            }
        }).on("loaded.rs.jquery.bootgrid", function(){
            $("a#command-edit").on("click", function(e){
                modal=$($(this).data("target"));
                reload=modal.find("a.reload");
                url = $(this).data("form-url");
                modal_body = modal.find(".modal-body");
                
                modal.find(".modal-title").text("Cargando...");
                modal.find("form.ajax").attr("action","/ajax/send/form/{{tname}}");
                
                modal_body.html("<p class='text-center'><i class='fa fa-fw fa-spinner fa-spin fa-3x'></i></p>")
                reload.attr("href",url); 
                modal.modal('show');
                
                reload.trigger("click");
                
            })  
        });
    </script>
</div>
"""


campos = ['id', 'usuario', 'password', 'permiso',"edit"]
consulta = ['id', 'usuario', 'password', 'permiso',"id as edit"]
nombres  = ['id', 'usuario', 'password', 'permiso',"Edicion"]
tabla = 'usuarios'
search="usuario like '%{phrase}%'"
usuarios = Table(tabla,campos,consulta,nombres,search)   
usuarios.set_url_query_table("/ajax/query/table/usuarios")
usuarios.set_url_query_form("/ajax/query/form/usuarios") 


rfid = Table("rfid")   
rfid.tfields = ['id', 'tag', "edit"]
rfid.theaders = ['ID', 'RFID', "Editar"] 
rfid.qfields = ['id', 'tag', "tag as edit"]
rfid.search = "tag like '%{phrase}%'"
rfid.set_url_query_table("/ajax/query/table/rfid") 
rfid.set_url_query_form("/ajax/query/form/rfid")  

    
@app.route('/ajax/cargatabla/')
def ajax_cargatabla(): 
    
    tname=request.GET.get('tname')
    print("tname: %s"%(tname))
    
    table=''
    if tname == "rfid":
        table = rfid.__str__()
        
    elif tname == "usuarios":
        table = usuarios.__str__()
        
    return {"inner-fragments": {"#table-edit":table}} 



@app.route('/ajax/query/table/<tname>', method="POST")
def ajax_query_table(tname): 
    current  = int(request.POST.get('current')) if request.POST.get('current')!='NaN' else 1
    rowCount = int(request.POST.get('rowCount')) if request.POST.get('rowCount')!='NaN' else 1
    if current < 0 : current =1
    if rowCount < 0 : rowCount =1
    
    if usuarios.is_table(tname):
        print("tabla USUARIOS") 
        total,current,rows = usuarios.db_get(current,rowCount,request.POST.get('searchPhrase'),request.POST)   
    elif rfid.is_table(tname):
        print("tabla RFID") 
        total,current,rows = rfid.db_get(current,rowCount,request.POST.get('searchPhrase'),request.POST)   
    
    return {
            "current": current+1, 
            "rowCount": len(rows),
            "rows": rows,
            "total": total
        }
    
    
def edit_usuario():
    f = """
    <div class="row form-group">
        <label class="control-label col-xs-4">Email</label>
        <div class="col-xs-8">
            <input type="email" class="form-control" placeholder="Email">
        </div>
    </div>

    <div class="row form-group">
        <label for="inputPassword" class="control-label col-xs-2">Password</label>
        <div class="col-xs-10">
            <input type="password" class="form-control" id="inputPassword" placeholder="Password">
        </div>
    </div>
    
    

    <div class="row form-group has-success has-feedback">
        <label class="col-xs-2 control-label" for="inputSuccess">Username</label>
        <div class="col-xs-10">
            <input type="text" id="inputSuccess" class="form-control" placeholder="Input with success">
        </div>
    </div>
    """
    f="""
    <form id="loginForm" method="post" class="form-horizontal">
    <div class="form-group">
        <label class="col-sm-4 control-label">Usuario</label>
        <div class="col-sm-4">
            <input type="text" class="form-control" name="username" />
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-4 control-label">Elija Contraseña</label>
        <div class="col-sm-4">
            <input type="password" class="form-control" name="password" />
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-5 col-sm-offset-3">
            <button type="submit" class="btn btn-default">Login</button>
        </div>
    </div>
    </form>
    """
    return f



    
@app.route('/ajax/query/form/<tname>/<id>')
def ajax_query_form(tname,id): 
    title =  "Hola mundo"
    body = "nada"
    script = ""
    
    if usuarios.is_table(tname):
        title = "Edicion de usuario"
        usuarios.db_query("edit = '%s'"%id)
        body = str(usuarios.db_query("edit = '%s'"%id))
        body = edit_usuario()
        print(body)

    
    elif rfid.is_table(tname):
        title = "Edicion de RFID"
        body = str(rfid.db_query("edit = '%s'"%id))
        print(body)
        
    return { "inner-fragments":{
                "#generic-modal .modal-title": title ,
                "#generic-modal .modal-body": body ,
                "#generic-modal script": script ,
                #"#modal-mensajes .modal-footer": footer,  
               } 
            }


@app.route('/ajax/send/form/<table>')
def ajax_send_form(table): 
    print("send table: %s"%(table))
    for x in request.GET:
        print(x,":",request.GET.get(x))
    

@app.route('/ajax/form/<table>/<id>')
def ajax_form(table,id): 
    print("id: %s - %s "%(table,id))
    for x in request.GET:
        print(x,":",request.GET.get(x))
    
    title="Modifiacion %s id:%s "%(table.upper(),id) 
    body = f_input(name="name",text="contrasena",type="password", required=True )
    footer = "nada 2"
    script = ""
    
    if table.upper() == "USUARIOS":
        print("tabla USUARIOS")
        campos = ['id', 'usuario', 'password', 'permiso']
        total,current,rows = db_get(1,1,campos,'usuarios',"id=%s"%id)
        print(rows)
        body = f_row( f_col('<p>id <span class="badge">%s</span><p>'%id,4,4,4) + f_col(sel,4,4,4) )
        script = """
            <script>
                $('.selectpicker').selectpicker('refresh');
            </script>
            """
        #body = '<p>id <span class="badge">%s</span><p>'%(id)
    
    
    sleep(1)
    return { "inner-fragments":{
                "#modal-mensajes .modal-title": title ,
                "#modal-mensajes .modal-body": body ,
                "#modal-mensajes script": script ,
                #"#modal-mensajes .modal-footer": footer, 
               } 
            } 


    
@app.route('/ajax/query/<query>', method="POST")
def ajax_modal(query): 
    print("query:%s"%(query))
    for x in request.POST:
        print(x,":",request.POST.get(x))
        
    """
        segun la conulta del ajax completar estos campos
            campos=[]
            tabla=''
            where=''
            order=''
            
        para buscar en db
        
    """
    

    current  = int(request.POST.get('current')) if request.POST.get('current')!='NaN' else 1
    rowCount = int(request.POST.get('rowCount')) if request.POST.get('rowCount')!='NaN' else 1
    if current < 0 : current =1
    if rowCount < 0 : rowCount =1
    
    if query == '1':
    #if usuarios.is_table(query):
        print("***** por aca!")
        total,current,rows = usuarios.db_get(current,rowCount,request.POST.get('searchPhrase'),request.POST)   
        
        print(rows)
        print("current",current)
        return {
            "current": current+1, 
            "rowCount": len(rows),
            "rows": rows,
            "total": total
            }

    if query == '1':
        campos = ['id', 'usuario', 'password', 'permiso']
        tabla = 'usuarios'
        searchPhrase=request.POST.get('searchPhrase')
        where="usuario like '%"+searchPhrase+"%'" if searchPhrase else ""
        print("where:",where)
        
        order=''
        if request.POST.get('sort[usuario]'):
            order = "order by usuario %s"%request.POST.get('sort[usuario]')  
        if request.POST.get('sort[id]'):
            order = "order by id %s"%request.POST.get('sort[id]')  
            
        campos = ['id', 'usuario', 'password', 'permiso']
        tabla = 'usuarios'
        search="usuario like '%{phrase}%'"
        a = Table(43,tabla,campos,search)
        print(a)

        print(a._where(searchPhrase))
        print(a._order(request.POST))
        
        
                
    elif query == '2':
        campos = ['id', 'tag']
        tabla = 'RFID'
        searchPhrase=request.POST.get('searchPhrase')
        where="tag like '%"+searchPhrase+"%'" if searchPhrase else ""
        print("where:",where)
        
        order=''
        if request.POST.get('sort[tag]'):
            order = "order by tag %s"%request.POST.get('sort[tag]')  
        if request.POST.get('sort[id]'):
            order = "order by id %s"%request.POST.get('sort[id]')   
            
            
    current  = int(request.POST.get('current')) if request.POST.get('current')!='NaN' else 1
    rowCount = int(request.POST.get('rowCount')) if request.POST.get('rowCount')!='NaN' else 1
    if current < 0 : current =1
    if rowCount < 0 : rowCount =1
    
    total,current,rows = db_get(current,rowCount,campos,tabla,where,order)   
    print(rows)
    print("current",current)
    return {
        "current": current+1,
        "rowCount": len(rows),
        "rows": rows,
        "total": total
        }


@app.route('/guardar/<table>', method="POST")
def guardar(table): 
    print("table:%s"%(table))
    for x in request.POST:
        print(x,":",request.POST.get(x))
    
    fields=["dni","nombre","apellido","rfid","telefono","Observaciones"]
    #for f in fields:
        #if f
        #values.append(request.POST.get(f))
        
    fields = ",".join(fields)
    sql = """
    insert into {tname} ({fields}) values ({values})
    """.format(tname="choferes",fields=fields,values=values)
    
    msg=""" 
    <div class="alert alert-info">
        <a href="#" class="close" data-dismiss="alert">&times;</a>
        <strong>Nota:</strong> Se guardo correctamente la informacion
        %s
    </div>
    """%sql
    return { "prepend-fragments":{ "#cargar-db .modal-body": msg }}
        
        
        
@app.route('/cargar-db')
def cargar_db(): 

    body="""
    <form  method="post" action="/guardar/chofer" class="ajax form-horizontal">
        <div class="form-group">
            <label class="col-sm-4 control-label">Apellido</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" name="apellido" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-4 control-label">Nombre</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" name="nombre" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-4 control-label">DNI</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" name="dni" pattern="\d{1,2}\.\d{1,3}\.\d{1,3}" title="12.123.123">
            </div>
        </div>

        
        <div class="form-group">
            <label class="col-sm-4 control-label">Tag RFID</label>
            <div class="col-sm-6">
                <select name="tag" class="selectpicker show-menu-arrow" data-width="100%" " data-live-search="true">
                    <option>2341234</option>
                    <option>2sfadsf</option>
                </select>
            </div>
        </div>
        
        <div class="form-group">
            <label class="col-sm-4 control-label">Telefono</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" name="telefono" />
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-4 control-label">Observaciones</label>
            <div class="col-sm-6"> 
                <textarea class="form-control" name="observaciones" rows="3" ></textarea>
            </div>
        </div>
        
        <div class="form-group">
            <div class="col-sm-5 col-sm-offset-4">
                <button type="submit" class="btn btn-primary">Guardar</button> 
            </div>
        </div>
    </form> 
    """ 

    title = "Edicion de Chofer"
    return { "inner-fragments":{
                "#cargar-db .modal-title": title ,
                "#cargar-db .modal-body": body
               } 
            } 


@app.route('/admin')
def index():
    return template('./templates/master_page.tpl', body=p_admin )

@app.route('/')
def index():
    body=html   
    return template('./templates/master_page.tpl', body=body )


@app.route('/<base>/<filepath:path>')
def server_static(base,filepath):
    return static_file(filepath, root=base)    

run(app, host='127.0.0.1', port=8989,reloader=True)
