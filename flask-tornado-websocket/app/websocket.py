# coding: utf-8
import json
from tornado.websocket import WebSocketHandler
import logging
import datetime
from threading import Thread
from time import sleep
from app import app,session


thread = None
broadcast_msg = []
subscribers = set() 

def mk_broadcast(msg):
    print ("mk_broadcast")
    for callback in subscribers:
        print(callback) 
        try:
            callback(json.dumps({
                    "output" : msg,
                    "time"    : str(datetime.datetime.today()),
                    "username" : "Jesse"
                })) 
        except:
            logging.error("Error in waiter callback", exc_info=True)


@app.route('/websocket/<path:path>') 
class WSHandler(WebSocketHandler):
    def open(self):
        logging.info("WebSocket opened!")
        subscribers.add(self.send_message)
        
    def send_message(self, message):
        self.write_message(message)

    def on_message(self, message):
        t=json.loads(message)
        print("t:",t['text'])
        mk_broadcast(t['text'])  

    def on_close(self):
        logging.info("WebSocket closed!")
        subscribers.remove(self.send_message)
        
