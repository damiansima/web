# coding: utf-8
from flask import render_template

from app import app,session
from app.websocket import WSHandler,mk_broadcast


@app.route('/')
def index():
    return render_template('index.html')



@app.route('/que/<que>')
def que(que):
    print("session:",session)
    mk_broadcast(que) 
    return "Hola %s"%que 


