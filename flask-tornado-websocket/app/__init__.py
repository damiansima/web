# coding: utf-8
import os
from flask import Flask
from flask import session,g

app = Flask(__name__)
app.secret_key = os.urandom(24)
app.debug = True

from app import views