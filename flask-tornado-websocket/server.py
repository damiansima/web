#!/usr/bin/env python
# coding: utf-8
from tornado.wsgi import WSGIContainer
from tornado.web import Application, FallbackHandler
from tornado.ioloop import IOLoop
from tornado import autoreload
from app.websocket import WSHandler
from app import app
import os.path

settings = dict(
    template_path=os.path.join(os.path.dirname(__file__), "/templates"),
    static_path=os.path.join(os.path.dirname(__file__), "/static"),
    debug=True
)

if __name__ == '__main__':
    #app.run(debug=True)
    
    wsgi_app = WSGIContainer(app)

    application = Application([
        (r'/websocket', WSHandler),
        (r'.*', FallbackHandler, dict(fallback=wsgi_app))
    ],settings)

    application.listen(5000)
    ioloop =IOLoop().instance()
    autoreload.start(ioloop)
    ioloop.start()
