from __future__ import print_function
import time
import socket
import sqlite3
import re


class DBconfig(object):
    def __init__(self):
        self.isconect=False	
        self.conn = None	
    
    def connect(self,dbname='dbconfig.db'):
        if not self.isconect: 
            self.conn = sqlite3.connect(dbname)	
            self.c = self.conn.cursor()
            self.isconect=True
            
    def initDB(self):
        if not self.isconect: 
            self.connect()
        
        self.c.execute('''CREATE TABLE USUARIOS(
            ID integer primary key autoincrement,
            USUARIO char(256) unique not null,
            PASSWORD char(80),
            PERMISO char(4)
            )''')
        self.c.execute('''CREATE TABLE RFID(
            ID integer primary key autoincrement,
            TAG varchar unique not null
            )''')
        
        self.c.execute('''CREATE TABLE CHOFERES(
            ID integer primary key autoincrement,
            Apellido char(80) not null,
            Nombre char(80) not null,
            DNI char(80),
            RFID char(80),
            Telefono char(80),
            Observaciones varchar)
            ''')
        
        
        self.c.execute('PRAGMA foreign_keys = ON')
        self.conn.commit()
        
    def close(self):
        if self.conn:
            # self.conn.commit()
            self.conn.close()
        self.conn = None
        self.isconect=False 
    
    def show(self, table=None):
        if not self.isconect: 
            self.connect()
        self.c.execute("select * FROM '%s'"%table)
        values = self.c.fetchall()
        return(values)
    
    def mkquery(self,campos,tabla,where,order):
        where = " where %s "%(where) if where else ""
        query='select %s from %s %s %s'%(','.join(campos),tabla,where,order)
        print("query:<<%s>>"%(query))
        self.c.execute(query)
    
    def fetchone(self):
        return self.c.fetchone()
    
    def fetchall(self):
        return self.c.fetchall()
    
    def query(self,pageNumber,rowCount,campos, tabla, where=None, order="" ):
        self.mkquery(campos,tabla,where,order)
        ret = self.fetchall()
        total = len(ret)
        maxrow = len(ret) if len(ret) < pageNumber*rowCount else pageNumber*rowCount
        pageCurrent = (int((maxrow-1)/rowCount))
        minrow = (pageCurrent)*rowCount
        
        return (total,pageCurrent,ret[minrow:maxrow])
        
        
        #for usuario in usuarios:
            #print("usuario:",usuario) 
        
        
if __name__ == '__main__':
    db=DBconfig()
    db.connect()
    try:
        db.initDB()
        #db.c.execute('INSERT INTO RFID VALUES(?,?)',(None,"12345w14afq24"))
        #db.c.execute('INSERT INTO RFID VALUES(?,?)',(None,"asfqw24232q24"))
        #db.c.execute('INSERT INTO RFID VALUES(?,?)',(None,"asfqw14afq224"))
        #db.c.execute('INSERT INTO RFID VALUES(?,?)',(None,"asfqw14a23424"))
        #db.conn.commit() 
    
    except:
        print('db.initDB() previo !')
        

    rowCount='1'
    searchPhrase=''
    current= '1'

    campos = ['ID', 'usuario', 'password', 'permiso']
    tabla = 'usuarios'
    
    
    print(db.query(1,1,campos,tabla))
    print(db.query(2,3,campos,tabla))
    print(db.query(3,3,campos,tabla))
    
    exit(0)

    campos = ['ID', 'usuario', 'password', 'permiso']
    tabla = 'usuarios'
    db.c.execute('select %s from %s'%(','.join(campos),tabla))
    usuarios = db.c.fetchall()
    print(usuarios)
    for usuario in usuarios:
        print("usuario:",usuario)