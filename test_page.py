#!/usr/bin/env python
# -*- encoding: CP850 -*-
### -*- encoding: CP850-8 -*-

from bottle import get
from bottle import post
from bottle import request
from bottle import Bottle
from bottle import run,static_file
from bottle import template

from bottle import response

app = Bottle()


def f_button(type="", mode="default", text=None, icon="",param="" ):
	if type=="submit":
		icon="fa fa-fw fa-download"
		if not text: text = "Guardar" 
	if type=="reset":
		icon="fa fa-fw fa-refresh"
		if not text: text = "Limpiar"
	if type=="close":
		type="button"
		icon="fa fa-fw fa-times"
		mode+=" btn-close"
		param='style="float: right;" data-dismiss="modal"'
		if not text: text = "Cerrar"
	return """ <button type="{type}" class="btn btn-{mode}" {param}><span class="{icon}"> </span> {text}</button> """.format(
			type=type,mode=mode,icon=icon,param=param,text=text)
				

def f_row(content):
	return """<div class="row">{content}</div>\n""".format(content=content)
			
def f_col(content,width_small=False,width_medium=False,width_long=False,offset=False):
	width=""
	if width_small : width+="col-sm-%s "%width_small
	if width_medium : width+="col-md-%s "%width_medium
	if width_long : width+="col-lg-%s "%width_long
	return """<div class="{width}">{content}</div>\n""".format(content=content,width=width)		

	
def f_input(name="name",text="...",id="", type="text", param="", classarg="", required=False ):
	if required: 
		param+=" required"
	return """<input id="{id}" class="form-control {classarg}" name="{name}" placeholder="{text}" type="{type}" {param} />
		""".format(name=name,id=id,text=text,type=type,param=param,classarg=classarg)


def f_select(name="name",options=[1,2],id="", param="", classarg="", size=5, search=False ):
	if search: 
		param+=' data-live-search="true"'
	options = "\n".join([ "<option>%s</option>"%i for i in options ]) 
	return """<select name="{name}" id="{id}" class="selectpicker show-menu-arrow {classarg}" {param} data-width="100%"  data-size="{size}">
		{options}</select>""".format(name=name,id=id,size=size,param=param,classarg=classarg,options=options)
		
	
sel = f_select(name="select1",options=["sima","yea men"],search=True)
	
print(f_button(type="submit"))
footer=f_button(type="submit",mode="primary")+f_button(mode="primary",type="reset")
#footer = f_button(mode="primary",text="",icon="fa fa-times", param='data-dismiss="modal"' ) 

button=f_button( mode="primary",text="modal",icon="glyphicon glyphicon-edit", param='data-toggle="modal" data-target="#contact"' ) 
test = f_col(button,9,5,2) + f_col(button,2,2,2)  + f_col(button,2,2,2)
button=f_button( mode="info",text="modal",icon="glyphicon glyphicon-edit", param='data-toggle="modal" data-target="#contact"' ) 
input = f_input(name="name",text="contrasena",type="password", required=True )
test2 = f_col(button,2,5,9) + f_col(sel,2,2,2) + f_col(input,2,5,2) 
button = f_row(test) + f_row(test2)
print(button)
		

	
test="""
<a class="btn btn-info" data-toggle="modal" data-target="#contact" >
	Contact
</a>

<i class="fa fa-spinner fa-spin"></i>
<i class="fa fa-circle-o-notch fa-spin"></i>
asd
<i class="fa fa-refresh fa-spin "></i>  
<i class="fa fa-cog fa-spin"></i>

<br>
	{button}
<div class="row">
  <div class="col-sm-9">
    Level 1: .col-sm-9
    <div class="row">
      <div class="col-xs-8 col-sm-6">
        Level 2: .col-xs-8 .col-sm-6
      </div>
      <div class="col-xs-4 col-sm-6">
        Level 2: .col-xs-4 .col-sm-6
      </div>
    </div>
  </div>
</div>
<br>

 <div class="modal fade" id="contact" tabindex="-1" role="dialog">
	<div class="modal-dialog">
		<div class="panel panel-primary">
			<div class="panel-heading">
						<button type="button" class="close" data-dismiss="modal" ><span class="fa fa-fw fa-times "> </span> </button>
						<button type="button" class="close"  ><span class="fa fa-fw  fa-download "> </span> </button>
						
				<h3 class="panel-title" id="contactLabel"> Hola mundo </h3>
			</div>
			<form role="form" class="ajax" action="/tablas/usuarios/send" method="get" data-reload-url="/tablas/usuarios/" accept-charset="utf-8">
				<div class="modal-body onchange" name="allforms" style="padding: 22px;">
				  <div class="row">
						<div class="col-lg-6 col-md-6 col-sm-6" >
							<select name="combo" class="selectpicker show-menu-arrow " data-width="100%" data-live-search="true" data-size="5">
								<option>Mustard</option>
								<option>Ketchup</option>
								<option>Relish</option>
								<option>Tent</option>
								<option>Flashlight</option>
								<option>Toilet Paper</option>
								<option>Tent</option>
								<option>Flashlight</option>
								<option>Toilet Paper</option>
							</select>
						</div>
						<div class="col-lg-6 col-md-6 col-sm-6" >
							<input class="form-control onffocus" name="email" placeholder="E-mail" type="text" required />
						</div>
					</div>
					<div class="row">
						<div class="col-lg-12 col-md-12 col-sm-12" >
							<input class="form-control" name="subject" placeholder="Subject" type="text" required />
						</div>
					</div>
					<div class="row">
						<div class="col-lg-4 col-md-4 col-sm-4" style="padding-bottom: 10px;"> 
						<input class="form-control input-lg" type="text" placeholder=".input-lg">
						</div>
						<div class="col-lg-4 col-md-4 col-sm-4" style="padding-bottom: 10px;"> 
						<input class="form-control" type="text" placeholder="Default input">
						</div>
						<div class="col-lg-4 col-md-4 col-sm-4" style="padding-bottom: 10px;"> 
						<input class="form-control input-sm" type="text" placeholder=".input-sm">
						</div>

					</div>
					<div class="row">
					
						<div class="col-lg-6 col-md-6 col-sm-6" >
							<input class="form-control onffocus" name="email" placeholder="E-mail" type="text" required />
						</div>
						<div class="col-lg-6 col-md-6 col-sm-6" >
							<input class="form-control onffocus" name="email" placeholder="E-mail" type="text" required />
						</div>
						  
						  
					</div>
					<div class="row">
						<div class="col-lg-12 col-md-12 col-sm-12">
							<textarea style="resize:vertical;" class="form-control" placeholder="Message..." rows="6" name="comment" required></textarea>
						</div>
					</div>
				</div>  
				<div class="panel-footer" style="margin-bottom:14px;">
					{footer}
				</div>
			</form>
		</div>
	</div>
</div> 
""".format(footer=footer,button=button)		


form="""
<form role="form" class="ajax" action="/tablas/usuarios/send" method="get" data-reload-url="/tablas/usuarios/" accept-charset="utf-8">
	<div class="modal-body onchange" name="allforms" style="padding: 22px;">
	  <div class="row">
			<div class="col-lg-6 col-md-6 col-sm-6" >
				<select name="combo" class="selectpicker show-menu-arrow " data-width="100%" data-live-search="true" data-size="5">
					<option>Mustard</option>
					<option>Ketchup</option>
					<option>Relish</option>
					<option>Tent</option>
					<option>Flashlight</option>
					<option>Toilet Paper</option>
					<option>Tent</option>
					<option>Flashlight</option>
					<option>Toilet Paper</option>
				</select>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6" >
				<input class="form-control onffocus" name="email" placeholder="E-mail" type="text" required />
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12" >
				<input class="form-control" name="subject" placeholder="Subject" type="text" required />
			</div>
		</div>
		<div class="row">
			<div class="col-lg-4 col-md-4 col-sm-4" style="padding-bottom: 10px;"> 
			<input class="form-control input-lg" type="text" placeholder=".input-lg">
			</div>
			<div class="col-lg-4 col-md-4 col-sm-4" style="padding-bottom: 10px;"> 
			<input class="form-control" type="text" placeholder="Default input">
			</div>
			<div class="col-lg-4 col-md-4 col-sm-4" style="padding-bottom: 10px;"> 
			<input class="form-control input-sm" type="text" placeholder=".input-sm">
			</div>

		</div>
		<div class="row">
		
			<div class="col-lg-6 col-md-6 col-sm-6" >
				<input class="form-control onffocus" name="email" placeholder="E-mail" type="text" required />
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6" >
				<input class="form-control onffocus" name="email" placeholder="E-mail" type="text" required />
			</div>
			  
			  
		</div>
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12">
				<textarea style="resize:vertical;" class="form-control" placeholder="Message..." rows="6" name="comment" required></textarea>
			</div>
		</div>
	</div>  
	<div class="panel-footer" style="margin-bottom:14px;">
		{footer}
	</div>
</form>
""" 

table="""
    <table id="grid-basic" class="table table-condensed table-hover table-striped">
    <thead>
    <tr>
    <th data-column-id="id" data-type="numeric">ID</th>
    <th data-column-id="sender">Sender</th>
    <th data-column-id="received" data-order="desc">Received</th>
    </tr>
    </thead>
    <tbody>
    <tr>
    <td>1</td>
    <td>eduardo@pingpong.com</td>
    <td>14.10.2013</td>
    </tr>
    
    <tr>
    <td>3</td>
    <td>eduardo@pingpong.com</td>
    <td>14.10.2013</td>
    </tr>
    
	<tr>
    <td>2</td>
    <td>eduardo@pingpong.com</td>
    <td>14.10.2013</td>
    </tr>
    
	<tr>
    <td>4</td>
    <td>eduardo@pingpong.com</td>
    <td>14.10.2013</td>
    </tr>
    
    </tbody>
    </table>
"""

table2="""
    <table id="grid-basic2" class="table table-condensed table-hover table-striped">
    <thead>
    <tr>
    <th data-column-id="id" data-type="numeric">ID</th>
    <th data-column-id="sender">Sender</th>
    <th data-column-id="received" data-order="desc">asdf</th>
    </tr>
    </thead>
    <tbody>
    <tr>
    <td>21</td>
    <td>eduardo@pingpong.com</td>
    <td>14.10.2013</td>
    </tr>
    
    <tr>
    <td>23</td>
    <td>eduardo@pingpong.com</td>
    <td>14.10.2013</td>
    </tr>
    
	<tr>
    <td>22</td>
    <td>eduardo@pingpong.com</td>
    <td>14.10.2013</td>
    </tr>
    
	<tr>
    <td>24</td>
    <td>eduardo@pingpong.com</td>
    <td>14.10.2013</td>
    </tr>
    
    </tbody>
    </table>
"""

html="""
<a class="btn btn-info" data-toggle="modal" data-target="#modal-mensajes" data-original-title>
    Mensajes
</a>

 <div class="modal fade" id="modal-mensajes" tabindex="-1" role="dialog" aria-labelledby="contactLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div id="genEditModal" class="modal-content">
			<div class="modal-header">
                <a type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</a>
                <h4 id="modal-title">Titulo</h4>
            </div>
            <div class="modal-body msg_info" >
                  
            </div>
        </div>
    </div>
</div>

<div class ="well" >
<a role="button" class="btn btn-info ajax" href="/ajax/modifica/modal" >
    modifiaca modal
</a>

<a role="button" class="btn btn-info" id="activselect">
    activar selectpicker
</a>

<a role="button" class="btn btn-info" id="eliminatabla">
    destroy
</a>

</div>

<div class ="well" id="msg_info2">
	%s
</div>


<script>
$("#activselect").click( function() {
        $('.selectpicker').selectpicker('refresh');
        $("#grid-basic").bootgrid({rowCount:10}); 
        $("#grid-basic2").bootgrid({rowCount:10}); 
    });
    
$("#eliminatabla").click( function() {
        $("#grid-basic").bootgrid("destroy"); 
        $("#grid-basic2").bootgrid('destroy'); 
    });
    
</script>"""%(table)

#.format(picker=f_select(name="select1",options=["sima","yea men"],search=True))


tr="""
<tr>
    <td>10238</td>
    <td>eduardo@pingpong.com</td>
    <td>14.10.2013</td>
</tr>
"""

@app.route('/ajax/modifica/modal')
def ajax_modal(): 
	msg="nada<br>"
	for x in request.GET:
		print(x)
		msg +="<strong>%s</strong>:%s<br>"%(x,request.GET.getall(x))
	msg_info="""
<div class="alert alert-block alert-success">
    <button type="button" class="close" data-dismiss="alert">
      <span aria-hidden="true">&times;</span> 
    </button>
    %s
</div>"""%(msg)
	
	return  {   "inner-fragments": 
                    {"#modal-mensajes .msg_info":form},
                "fragments": 
					{"#grid-basic":table2},
                "append-fragments": 
                    {"#msg_info2":sel}
            } 

@app.route('/test')
def index():
	body = test
	return template('./templates/master_page.tpl', user='',body=body )

@app.route('/')
def index():
	body=html	
	return template('./templates/master_page.tpl', user='',body=body )


@app.route('/<base>/<filepath:path>')
def server_static(base,filepath):
    return static_file(filepath, root=base)    

run(app, host='127.0.0.1', port=8989,reloader=True)
