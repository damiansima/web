<!DOCTYPE html>
<html>
    <head>
        <title>Modal y Ajax</title>
        <meta name="description" content="Prueba modal y ajax">
        <meta name="author" content="Sima">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        
        <link href="./css/bootstrap.min.css" rel="stylesheet">
        <link href="./css/bootstrap-theme.min.css" rel="stylesheet">
        <link href="./css/bootstrap-select.min.css" rel="stylesheet">
        <link href="./css/jquery.bootgrid.css" rel="stylesheet">
        <!-- <link href="./css/bootstrap-table.min.css" rel="stylesheet">-->
        <link href="./css/font-awesome.min.css" rel="stylesheet">


        <script src="./js/jquery.min.js"></script> 
        <script src="./js/bootstrap.min.js"></script>
        <script src="./js/bootstrap-typeahead.js"></script>
        <script src="./js/bootstrap-select.min.js"></script>

        <script src="./js/eldarion-ajax-core.js"></script>
        <script src="./js/eldarion-ajax-handlers.js"></script> 

        <script src="./js/jquery.bootgrid.js"></script>
        <!-- <script src="./js/bootstrap-table.min.js"></script>-->
		
		<style>
		form .row{
			margin-top: 10px; 
		}
		</style>

    </head>

    <body class="container">
    
    {{!body}}
    
    </body>
</html>