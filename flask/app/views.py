#!/usr/bin/env python
# -*- coding: utf-8 -*-

from flask import render_template, flash, redirect,url_for,session
from app import app,db
from .forms import *
from .models import *
from flask.ext.login import login_user, logout_user, current_user, login_required


@app.route('/')
@app.route('/index')
def index():
    email = session['email'] if 'email' in session else "Pepe"
    print( "email", email ) 
    user = {'name': email }
    posts = [
        {
            'author': {'nickname': 'John'},
            'body': 'Beautiful day in Portland!'
        },
        {
            'author': {'nickname': 'Susan'},
            'body': 'The Avengers movie was so cool!'
        }
    ]
    
    return render_template('index.html',
                           title='Home',
                           user=user,
                           posts=posts)


@app.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        if user is None:     
            user = User( form.email.data )
            db.session.add(user)
            db.session.commit()
            session['email']=user.email 
            print("login",user.email)
        return redirect(url_for('login'))
    return render_template('login.html',
                           title='Sign In',
                           form=form) 


@app.route('/logout')
def logout():
    return redirect(url_for('index'))



@app.route('/login2', methods=['GET', 'POST'])
def login2():
    name = None
    form = NameForm()
    if form.validate_on_submit():
        flash('Usuario %s'%form.name.data)
        name = form.name.data
        form.name.data = ''
        return redirect(url_for('login2'))
    return render_template('login.html',
                           title='Sign In',
                           form=form) 


@app.route('/basic')
def basic():
    email = session['email'] if 'email' in session else "Pepe"
    print( "email", email ) 
    user = {'name': email }
    return render_template('basic.html',
                           title='Home',
                           user=user)


@app.route('/tablas')
def visor_tablas():
	email = session['email'] if 'email' in session else "Pepe"
	print( "email", email ) 
	user = {'name': email }
	return render_template('visor_tablas.html',
                           title='Home',
                           user=user)
