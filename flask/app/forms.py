#!/usr/bin/env python
# -*- coding: utf-8 -*-

from flask.ext.wtf import Form
from wtforms.validators import DataRequired, Required, Email
from wtforms import StringField,TextField, HiddenField, ValidationError, RadioField,\
    BooleanField, SubmitField, IntegerField, FormField, validators, PasswordField


class LoginForm(Form):
    email = StringField('E-mail', validators=[Email()])
    password = PasswordField('Ingrese la Contrasena', validators=[DataRequired()])
    remember_me = BooleanField('Recordar', default=False)
    submit_button = SubmitField('Ingresar')


class NameForm(Form):
    name = StringField('What is your name?', validators=[Required()])
    submit = SubmitField('Submit') 
    
    
