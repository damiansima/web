#!/usr/bin/env python
# -*- coding: utf-8 -*-

from flask import render_template, flash, redirect,url_for,session
from app import app
from .forms import *
# from .models import *
from flask.ext.login import login_user, logout_user, current_user, login_required

from flask.ext.triangle import Triangle
Triangle(app)

@app.route('/')
@app.route('/index')
def index():
    email = session['email'] if 'email' in session else "Pepe"
    print( "email", email ) 
    user = {'name': email }
    posts = [
        {
            'author': {'nickname': 'John'},
            'body': 'Beautiful day in Portland!'
        },
        {
            'author': {'nickname': 'Susan'},
            'body': 'The Avengers movie was so cool!'
        }
    ]
    
    return render_template('index.html',
                           title='Home',
                           user=user,
                           posts=posts)


@app.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    return render_template('login.html',
                           title='Sign In',
                           form=form) 

@app.route('/login2', methods=['GET', 'POST'])
def login2():
    form = Profile("profile")
    return render_template('login.html',
                           title='Sign In',
                           form=form) 


@app.route('/basic')
def basic():
    email = session['email'] if 'email' in session else "Pepe"
    print( "email", email ) 
    user = {'name': email }
    return render_template('basic.html',
                           title='Home',
                           user=user)

