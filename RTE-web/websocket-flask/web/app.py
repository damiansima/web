 
from flask import Flask, render_template
from flask import request

# from flask.ext.triangle import Triangle, Form
# from flask.ext.triangle.widgets.standard import TextInput

app = Flask(__name__)

from flask_wtf import Form
from wtforms import StringField
from wtforms.validators import DataRequired

class MyForm(Form):
    name = StringField('name', validators=[DataRequired()])
    
    
        
# class Profile(Form):
    # fname = TextInput('profile.fname', label='First Name', required=True)
    # lname = TextInput('profile.lname', label='Last Name', required=True)
    # bdate = TextInput('profile.bdate', label='Birthdate', required=True)
    # bio = TextInput('profile.bio', label='Bio')    
    
##
## Our WSGI application .. in this case Flask based
##


    

@app.route('/profile')
def profile():
    print("aca...")
    # return "Hola mundo !"
    form = MyForm(csrf_enabled=False)
    print("form %s"%form)
    return render_template('profile.html', form=form)

@app.route('/')
def page_home():
    return "Hola mundo !"


if __name__ == '__main__':
    app.run(host='', port=8080,debug=True)

