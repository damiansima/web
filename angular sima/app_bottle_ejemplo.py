#!/usr/bin/env python
# -*- encoding: utf-8 -*-

from bottle import get
from bottle import post
from bottle import request
from bottle import Bottle
from bottle import run,static_file
from bottle import template

from bottle import response
from time import sleep
import json

app = Bottle()

html='''
<div ng-app="ui.bootstrap.demo" ng-controller="apiAppCtrl as vm">
    <h1>Pruebo Ajax con JSONP</h1>
    <p>
        Busca cerveza:
        <input type="text" ng-model="vm.nombre"> <input type="button" value="Buscar" ng-click="vm.buscaCervezas()">
    </p>
    <aside>
         <h2>Filtra:</h2>
          <input type="text" ng-model="vm.filtroCliente">
         <h2>Orden</h2>
          <p>
              <button ng-click="vm.orden=false">Alfabetico</button>
              <br />
              <button ng-click="vm.orden=true">Contrario</button>
          </p>
          <p>
              <input type="radio" name="campo" ng-model="vm.campo" value="name"> Nombre
              <br />
              <input type="radio" name="campo" ng-model="vm.campo" value="description"> Descripción
          </p>
          <p>vm.campo: {{vm.campo}}</p>
    </aside>

    <div class="table">
        <table class="table table-condensed table-striped table-bordered table-hover no-margin">
            <thead>
                <tr>
                    <th style="width:5%">Index </th>
                    <th style="width:10%">
                        <spam href="" ng-click="vm.orden=!vm.orden; vm.campo='name'">Name 
                        <spam ng-if="vm.campo=='name'"  class='icon glyphicon' ng-class="{'glyphicon-chevron-up': vm.orden, 'glyphicon-chevron-down': !vm.orden}"></spam>    
                        </spam>
                    </th>
                    <th >
                        <spam href="" ng-click="vm.orden=!vm.orden; vm.campo='description'">Description 
                        <spam ng-if="vm.campo=='description'"  class='icon glyphicon' ng-class="{'glyphicon-chevron-up': vm.orden, 'glyphicon-chevron-down': !vm.orden}"></spam>    
                        </spam>
                    </th>
                </tr>
            </thead>
            <tbody>
                <tr ng-repeat="cerveza in vm.cervezas | filter:vm.filtroCliente | orderBy:vm.campo:vm.orden" ng-class="{even: $even, odd: $odd}">
                    <td> <span class="label label label-info">{{$index +1}}</span> </td>
                    <td> <span class="name">{{cerveza.name}}</span> </td>
                    <td> <span class="name">{{cerveza.description}}</span> </td>
                </tr>
            </tbody>
        </table> 
    </div>
</div>
'''

respuesta={}
respuesta['beers']=[
    { 'name':"Quilmes", 'description':"la cerveza 1" },
    { 'name':"Palermo", 'description':"la cerveza 2" },
    { 'name':"Brama", 'description':"la cerveza 3" },
];

@app.get('/json/table/<tname>')
def ajax_send_form(tname): 
    print("request table: %s"%(tname))
    for x in request.GET:
        print(x,":",request.GET.get(x)) 
    return respuesta
    
@app.route('/')
def index():
    body=html   
    return template('templates/master_page.tpl',body=body)


@app.route('/<base>/<filepath:path>')
def server_static(base,filepath):
    return static_file(filepath, root=base)    

run(app, host='127.0.0.1', port=8989,reloader=True) 
