angular.module('ui.bootstrap.demo', ['ui.bootstrap']);

angular.module('ui.bootstrap.demo').controller('ButtonsCtrl', function ($scope) {
  $scope.singleModel = 1;

  $scope.radioModel = 'Middle';

  $scope.checkModel = {
    left: false,
    middle: true,
    right: false
  };
});

angular.module('ui.bootstrap.demo').controller('AlertDemoCtrl', function ($scope) {
  $scope.alerts = [
    { type: 'danger', msg: 'Oh snap! Change a few things up and try submitting again.' },
    { type: 'success', msg: 'Well done! You successfully read this important alert message.' }
  ];

  $scope.addAlert = function() {
    $scope.alerts.push({msg: 'Another alert!'});
  };

  $scope.closeAlert = function(index) {
    $scope.alerts.splice(index, 1);
  };
});

angular.module('ui.bootstrap.demo').controller('DatepickerDemoCtrl', function ($scope) {
  $scope.today = function() {
    $scope.dt = new Date();
  };
  $scope.today();

  $scope.clear = function () {
    $scope.dt = null;
  };

  // Disable weekend selection
  $scope.disabled = function(date, mode) {
    return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
  };

  $scope.toggleMin = function() {
    $scope.minDate = $scope.minDate ? null : new Date();
  };
  $scope.toggleMin();

  $scope.open = function($event) {
    $event.preventDefault();
    $event.stopPropagation();

    $scope.opened = true;
  };

  $scope.dateOptions = {
    formatYear: 'yy',
    startingDay: 1
  };

  $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
  $scope.format = $scope.formats[0];
});

angular.module('ui.bootstrap.demo').controller('ExampleController', ['$scope', function($scope) {
      $scope.example = {
        value: 12
      };
}]);


angular.module('ui.bootstrap.demo').controller('apiAppCtrl', controladorPrincipal);

function controladorPrincipal($scope, $http){
    var vm=this;

    vm.orden = false;
    vm.campo = "name";
    
    vm.buscaCervezas = function(){
        var url = "http://api.openbeerdatabase.com/v1/beers.json?callback=JSON_CALLBACK";
//        var url = "json/table/test";
        if(vm.nombre){
            url += "?query=" + vm.nombre
        };
//        $http.jsonp(url).success(function(respuesta){
        $http.jsonp(url).success(function(respuesta){
            console.log("res:", respuesta);
            vm.cervezas = respuesta.beers;
//            $('#table-transform1').bootstrapTable();
        });
    };
    
    vm.updateX = function(){
        $(document).ready(function() {
            $('#ejemplo-datatable1_1').dataTable( {
                "scrollY":        "200px",
                "scrollCollapse": true,
                "paging":         false
            } );
        var $table = $('#ejemplo-datatable1');
        $table.floatThead();
        $table.floatThead('reflow');
        } );
    };

    
};
