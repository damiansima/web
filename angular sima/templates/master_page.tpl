<!DOCTYPE html>
<html>
    <head>
        <title>Pruebas AngularJS - UI Bootstrap</title>
        <meta name="description" content="Prueba modal y ajax">
        <meta name="author" content="Sima">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <link href="/static/css/bootstrap.css" rel="stylesheet">
        <!--<link href="/static/css/bootstrap-theme.css" rel="stylesheet">-->
        <!--<link href="/static/css/font-awesome.min.css" rel="stylesheet">-->

        <!--<script src="/static/js/jquery.min.js"></script>-->
        <script src="/static/js/angular.min.js"></script>
        <script src="/static/js/ui-bootstrap-tpls-0.12.1.min.js"></script>
        <!--<script src="/static/js/bootstrap.min.js"></script>-->
        
        <script src="/static/js/test1.js"></script>
        
		
        <style>
        form .row{
                margin-top: 10px; 
        }
        </style>

    </head>

    <body class="container">
        <h1>Pruebas AngularJS - UI Bootstrap</h1>
        
        {{!body}}
    
    </body>
</html>