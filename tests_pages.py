#!/usr/bin/env python
# -*- encoding: CP850 -*-
### -*- encoding: CP850-8 -*-

from bottle import get
from bottle import post
from bottle import request
from bottle import Bottle
from bottle import run,static_file
from bottle import template

from bottle import response
from db.db import DBconfig
from time import sleep

app = Bottle()


def f_button(type="", mode="default", text=None, icon="",param="" ):
    if type=="submit":
        icon="fa fa-fw fa-download"
        if not text: text = "Guardar" 
    if type=="reset":
        icon="fa fa-fw fa-refresh"
        if not text: text = "Limpiar"
    if type=="close":
        type="button"
        icon="fa fa-fw fa-times"
        mode+=" btn-close"
        param='style="float: right;" data-dismiss="modal"'
        if not text: text = "Cerrar"
    return """ <button type="{type}" class="btn btn-{mode}" {param}><span class="{icon}"> </span> {text}</button> """.format(
            type=type,mode=mode,icon=icon,param=param,text=text)
                

def f_row(content):
    return """<div class="row">{content}</div>\n""".format(content=content)
            
def f_col(content,width_small=False,width_medium=False,width_long=False,offset=False):
    width=""
    if width_small : width+="col-sm-%s "%width_small
    if width_medium : width+="col-md-%s "%width_medium
    if width_long : width+="col-lg-%s "%width_long
    return """<div class="{width}">{content}</div>\n""".format(content=content,width=width)     

    
def f_input(name="name",text="...",id="", type="text", param="", classarg="", required=False ):
    if required: 
        param+=" required"
    return """<input id="{id}" class="form-control {classarg}" name="{name}" placeholder="{text}" type="{type}" {param} />
        """.format(name=name,id=id,text=text,type=type,param=param,classarg=classarg)


def f_select(name="name",options=[1,2],id="", param="", classarg="", size=5, search=False ):
    if search: 
        param+=' data-live-search="true"'
    options = "\n".join([ "<option>%s</option>"%i for i in options ]) 
    return """<select name="{name}" id="{id}" class="selectpicker show-menu-arrow {classarg}" {param} data-width="100%"  data-size="{size}">
        {options}</select>""".format(name=name,id=id,size=size,param=param,classarg=classarg,options=options)
        
    
sel = f_select(name="select1",options=["sima","yea men"],search=True)
    
print(f_button(type="submit"))
footer=f_button(type="submit",mode="primary")+f_button(mode="primary",type="reset")
#footer = f_button(mode="primary",text="",icon="fa fa-times", param='data-dismiss="modal"' ) 

button=f_button( mode="primary",text="modal",icon="glyphicon glyphicon-edit", param='data-toggle="modal" data-target="#contact"' ) 
test = f_col(button,9,5,2) + f_col(button,2,2,2)  + f_col(button,2,2,2)
button=f_button( mode="info",text="modal",icon="glyphicon glyphicon-edit", param='data-toggle="modal" data-target="#contact"' ) 
input = f_input(name="name",text="contrasena",type="password", required=True )
test2 = f_col(button,2,5,9) + f_col(sel,2,2,2) + f_col(input,2,5,2) 
button = f_row(test) + f_row(test2)
print(button)
        

    
test="""
<a class="btn btn-info" data-toggle="modal" data-target="#contact" >
    Contact
</a>

<i class="fa fa-spinner fa-spin"></i>
<i class="fa fa-circle-o-notch fa-spin"></i>
asd
<i class="fa fa-refresh fa-spin "></i>  
<i class="fa fa-cog fa-spin"></i>

<br>
    {button}
<div class="row">
  <div class="col-sm-9">
    Level 1: .col-sm-9
    <div class="row">
      <div class="col-xs-8 col-sm-6">
        Level 2: .col-xs-8 .col-sm-6
      </div>
      <div class="col-xs-4 col-sm-6">
        Level 2: .col-xs-4 .col-sm-6
      </div>
    </div>
  </div>
</div>
<br>

 <div class="modal fade" id="contact" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="panel panel-primary">
            <div class="panel-heading">
                        <button type="button" class="close" data-dismiss="modal" ><span class="fa fa-fw fa-times "> </span> </button>
                        <button type="button" class="close"  ><span class="fa fa-fw  fa-download "> </span> </button>
                        
                <h3 class="panel-title" id="contactLabel"> Hola mundo </h3>
            </div>
            <form role="form" class="ajax" action="/tablas/usuarios/send" method="get" data-reload-url="/tablas/usuarios/" accept-charset="utf-8">
                <div class="modal-body onchange" name="allforms" style="padding: 22px;">
                  <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6" >
                            <select name="combo" class="selectpicker show-menu-arrow " data-width="100%" data-live-search="true" data-size="5">
                                <option>Mustard</option>
                                <option>Ketchup</option>
                                <option>Relish</option>
                                <option>Tent</option>
                                <option>Flashlight</option>
                                <option>Toilet Paper</option>
                                <option>Tent</option>
                                <option>Flashlight</option>
                                <option>Toilet Paper</option>
                            </select>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6" >
                            <input class="form-control onffocus" name="email" placeholder="E-mail" type="text" required />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12" >
                            <input class="form-control" name="subject" placeholder="Subject" type="text" required />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-4" style="padding-bottom: 10px;"> 
                        <input class="form-control input-lg" type="text" placeholder=".input-lg">
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4" style="padding-bottom: 10px;"> 
                        <input class="form-control" type="text" placeholder="Default input">
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4" style="padding-bottom: 10px;"> 
                        <input class="form-control input-sm" type="text" placeholder=".input-sm">
                        </div>

                    </div>
                    <div class="row">
                    
                        <div class="col-lg-6 col-md-6 col-sm-6" >
                            <input class="form-control onffocus" name="email" placeholder="E-mail" type="text" required />
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6" >
                            <input class="form-control onffocus" name="email" placeholder="E-mail" type="text" required />
                        </div>
                          
                          
                    </div>
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <textarea style="resize:vertical;" class="form-control" placeholder="Message..." rows="6" name="comment" required></textarea>
                        </div>
                    </div>
                </div>  
                <div class="panel-footer" style="margin-bottom:14px;">
                    {footer}
                </div>
            </form>
        </div>
    </div>
</div> 
""".format(footer=footer,button=button)     


form="""
<form role="form" class="ajax" action="/tablas/usuarios/send" method="get" data-reload-url="/tablas/usuarios/" accept-charset="utf-8">
    <div class="modal-body onchange" name="allforms" style="padding: 22px;">
      <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6" >
                <select name="combo" class="selectpicker show-menu-arrow " data-width="100%" data-live-search="true" data-size="5">
                    <option>Mustard</option>
                    <option>Ketchup</option>
                    <option>Relish</option>
                    <option>Tent</option>
                    <option>Flashlight</option>
                    <option>Toilet Paper</option>
                    <option>Tent</option>
                    <option>Flashlight</option>
                    <option>Toilet Paper</option>
                </select>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6" >
                <input class="form-control onffocus" name="email" placeholder="E-mail" type="text" required />
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12" >
                <input class="form-control" name="subject" placeholder="Subject" type="text" required />
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-4" style="padding-bottom: 10px;"> 
            <input class="form-control input-lg" type="text" placeholder=".input-lg">
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4" style="padding-bottom: 10px;"> 
            <input class="form-control" type="text" placeholder="Default input">
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4" style="padding-bottom: 10px;"> 
            <input class="form-control input-sm" type="text" placeholder=".input-sm">
            </div>

        </div>
        <div class="row">
        
            <div class="col-lg-6 col-md-6 col-sm-6" >
                <input class="form-control onffocus" name="email" placeholder="E-mail" type="text" required />
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6" >
                <input class="form-control onffocus" name="email" placeholder="E-mail" type="text" required />
            </div>
              
              
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <textarea style="resize:vertical;" class="form-control" placeholder="Message..." rows="6" name="comment" required></textarea>
            </div>
        </div>
    </div>  
    <div class="panel-footer" style="margin-bottom:14px;">
        {footer}
    </div>
</form>
""" 

table="""
    <div id="table-edit">
        <table class="table table-condensed table-hover table-striped">
        <thead>
        <tr>
        <th data-column-id="id" data-type="numeric">ID</th>
        <th data-column-id="sender">Sender</th>
        <th data-column-id="received" data-order="desc">Received</th>
        </tr>
        </thead>
        <tbody>
        <tr>
        <td>1</td>
        <td>eduardo@pingpong.com</td>
        <td>14.10.2013</td>
        </tr>
        
        <tr>
        <td>3</td>
        <td>eduardo@pingpong.com</td>
        <td>14.10.2013</td>
        </tr>
        
        <tr>
        <td>2</td>
        <td>eduardo@pingpong.com</td>
        <td>14.10.2013</td>
        </tr>
        
        <tr>
        <td>4</td>
        <td>eduardo@pingpong.com</td>
        <td>14.10.2013</td>
        </tr>
        
        </tbody>
        </table>
        
        <script>
            $("#table-edit table").bootgrid("destroy");
            $("#table-edit table").bootgrid({rowCount:20});  
        </script>
    </div>
"""

table2="""
    <div id="table-edit">
        <table class="table table-condensed table-hover table-striped">
        <thead>
            <tr>
            <th data-column-id="id" data-type="numeric">ID</th>
            <th data-column-id="sender">Sender</th>
            <th data-column-id="received" data-order="desc">asdf</th>
            </tr>
        </thead>
        <tbody>
            <tr>
            <td>21</td>
            <td>eduardo@pingpong.com</td>
            <td>14.10.2013</td>
            </tr>
            
            <tr>
            <td>23</td>
            <td>eduardo@pingpong.com</td>
            <td>14.10.2013</td>
            </tr>
            
            <tr>
            <td>22</td>
            <td>eduardo@pingpong.com</td>
            <td>14.10.2013</td>
            </tr>
            
            <tr>
            <td>24</td>
            <td>eduardo@pingpong.com</td>
            <td>14.10.2013</td>
            </tr>
        </tbody>
        </table>
        
        <script>
            $("#table-edit table").bootgrid("destroy");
            $("#table-edit table").bootgrid({rowCount:20});  
        </script>
    </div>
"""

html="""
<a class="btn btn-info" data-toggle="modal" data-target="#modal-mensajes" data-original-title>
    Mensajes
</a>

 <div class="modal fade" id="modal-mensajes" tabindex="-1" role="dialog" aria-labelledby="contactLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div id="genEditModal" class="modal-content">
            <div class="modal-header">
                <a type="button" class="close" data-dismiss="modal" > <span class="fa fa-fw fa-times"> </span>  </a>
                <a type="button" class="ajax close reload"  href="/ajax/modifica/modal" > <span class="fa fa-fw fa-refresh"> </span>  </a>
                <h4 class="modal-title">Titulo</h4>
            </div>
            <form class="ajax" method="get" action="" data-reload-url="">
                <div class="modal-body" > 
                    asd
                </div>
                <div class="modal-footer" >  
                    <input type="submit" class="btn btn-success" value="Send"/>
                </div>
            </form>
        </div>
    </div>
</div>

<div class ="well" >
<a role="button" class="btn btn-info ajax" href="/ajax/modifica/modal" >
    modifiaca modal
</a>

<a role="button" class="btn btn-info" id="activselect">
    activar selectpicker
</a>

<a role="button" class="btn btn-info" id="eliminatabla">
    destroy
</a>

</div>

<div id="msg_info2">
    %s
</div>


<script>
$("#activselect").click( function() {
        $('.selectpicker').selectpicker('refresh');
        $("#table-edit table").bootgrid({rowCount:10});
    });
    
$("#eliminatabla").click( function() {
        $("#table-edit table").bootgrid("destroy"); 
    });
    
</script>"""%(table)

#.format(picker=f_select(name="select1",options=["sima","yea men"],search=True))

theader = [('id','ID'), ('usuario','Usuario'), ('password','Contrasena'), ('permiso','Permiso')]
tbody = [(1, 'sima', 'sima', '777'), (2, 'damian', 'sima', '777'), (3, 'pepe', 'sima', '777'), (4, 'guille', 'sima', '777'), (5, 'tapa', 'sima', '777'), (6, 'taqua', 'sima', '777'), (7, 'rama', 'sima', '777'), (8, 'rasd', 'sima', '777')]


_table2="""
<div id="table-edit" class="">
    <table class="table table-condensed table-hover" name="{{tname}}">
    <thead>
        <tr>
        % for i,v in theader: 
            <th data-column-id="{{i}}">{{v}}</th>
        % end
        <th data-column-id="edit" data-sortable="false" data-formatter="edit">Editar</th>
        </tr>
    </thead>
    <tbody>
        % for r in tbody:
            <tr>
            % for f in r:
                <td>{{f}}</td>
            % end
                <td data-edit-id="{{r[0]}}"> 
                    <a href="{{r[0]}}">editar {{r[0]}} </a>
                </td>
            </tr>
        % end
    </tbody>
    </table>
    
    <script>
        $("#table-edit table").bootgrid("destroy");
        $("#table-edit table").bootgrid({
            rowCount: 20,
            formatters: {
                edit: function (column, row){
                    return '<a type="button" class="btn btn-xs btn-default" data-row-id="' + row.id + '"><span class="fa fa-pencil"> </span> </button> ';
                }
            }
        });  
    </script>
</div>
"""


_table_ajax="""
<div id="table-edit" class="">
    <table class="table table-condensed table-hover" name="{{tname}}">
    <thead>
        <tr>
        % for i,v in theader: 
            <th data-column-id="{{i}}">{{v}}</th>
        % end
        <th data-column-id="edit" data-sortable="false" data-formatter="edit">Editar</th>
        </tr>
    </thead>
    </table>
    
    <script>
        $("#table-edit table").bootgrid("destroy");
        $("#table-edit table").bootgrid({
            rowCount: 3,
            ajax: true,
            url: "/ajax/query/{{tquery}}",
            formatters: {
                edit: function (column, row){
                    return '<a id="command-edit" class="btn btn-xs" data-form-url="/ajax/form/{{tname}}/'+ row.id +'" data-target="#{{modalid}}"><span class=\"fa fa-fw fa-pencil\"></span> </a>';
                    
                }
            }
        }).on("loaded.rs.jquery.bootgrid", function(){
            $("a#command-edit").on("click", function(e){
                modal=$($(this).data("target"));
                reload=modal.find("a.reload");
                url = $(this).data("form-url");
                modal_body = modal.find(".modal-body");
                
                modal.find(".modal-title").text("Cargando...");
                modal_body.html("<p class='text-center'><i class='fa fa-fw fa-spinner fa-spin fa-3x'></i></p>")
                reload.attr("href",url);
                modal.modal('show');
                
                reload.trigger("click");
                
            })  
        });
    </script>
</div>
"""


t2 = template(_table_ajax, tname='usuarios', theader=theader, tquery="1", modalid="modal-mensajes" )
theader= [('id','ID'), ('tag','RFID')]
t2 = template(_table_ajax, tname='rfid', theader= theader, tquery="2", modalid="modal-mensajes" )
print(t2)

@app.route('/ajax/modifica/modal')
def ajax_modal(): 
    msg="nada<br>"
    for x in request.GET:
        print(x)
        msg +="<strong>%s</strong>:%s<br>"%(x,request.GET.getall(x))
    msg_info="""
<div class="alert alert-block alert-success">
    <button type="button" class="close" data-dismiss="alert">
      <span aria-hidden="true">&times;</span> 
    </button>
    %s
</div>"""%(msg)
    
    return  {   "inner-fragments": 
                    {"#modal-mensajes .msg_info":form},
                "fragments": 
                    {"#table-edit":t2},
                "append-fragments": 
                    {"#msg_info2":sel}
            } 

@app.route('/ajax/form/<table>/<id>')
def ajax_form(table,id): 
    print("id: %s - %s "%(table,id))
    for x in request.GET:
        print(x,":",request.GET.get(x))
    
    title="Modifiacion RFID id:%s "%(id) 
    body = "nada" 
    footer = "nada 2"
    
    sleep(1)
    return { "inner-fragments":{
                "#modal-mensajes .modal-title": title ,
                "#modal-mensajes .modal-body": body ,
                #"#modal-mensajes .modal-footer": footer, 
               } 
            } 


def db_get(pageNumber,rowCount,campos,tabla,where,order=""):
    db=DBconfig()
    db.connect('db/dbconfig.db')
    total,current,res = db.query(pageNumber,rowCount,campos,tabla,where,order)
    db.close() 
    rows=[]
    for row in res: rows.append({ c:row[i] for i,c in enumerate(campos)})
    return (total,current,rows)
    
    
    
@app.route('/ajax/query/<query>', method="POST")
def ajax_modal(query): 
    print("query:%s"%(query))
    for x in request.POST:
        print(x,":",request.POST.get(x))
        
    """
        segun la conulta del ajax completar estos campos
            campos=[]
            tabla=''
            where=''
            order=''
            
        para buscar en db
        
    """
    if query == '1':
        campos = ['id', 'usuario', 'password', 'permiso']
        tabla = 'usuarios'
        searchPhrase=request.POST.get('searchPhrase')
        where="usuario like '%"+searchPhrase+"%'" if searchPhrase else ""
        print("where:",where)
        
        order=''
        if request.POST.get('sort[usuario]'):
            order = "order by usuario %s"%request.POST.get('sort[usuario]')  
        if request.POST.get('sort[id]'):
            order = "order by id %s"%request.POST.get('sort[id]')   
                
    elif query == '2':
        campos = ['id', 'tag']
        tabla = 'RFID'
        searchPhrase=request.POST.get('searchPhrase')
        where="tag like '%"+searchPhrase+"%'" if searchPhrase else ""
        print("where:",where)
        
        order=''
        if request.POST.get('sort[tag]'):
            order = "order by tag %s"%request.POST.get('sort[tag]')  
        if request.POST.get('sort[id]'):
            order = "order by id %s"%request.POST.get('sort[id]')   
            
            
    current  = int(request.POST.get('current')) if request.POST.get('current')!='NaN' else 1
    rowCount = int(request.POST.get('rowCount')) if request.POST.get('rowCount')!='NaN' else 1
    if current < 0 : current =1
    if rowCount < 0 : rowCount =1
    
    total,current,rows = db_get(current,rowCount,campos,tabla,where,order)   
    print(rows)
    print("current",current)
    return {
        "current": current+1,
        "rowCount": len(rows),
        "rows": rows,
        "total": total
        }




@app.route('/test')
def index():
    body = test
    return template('./templates/master_page.tpl', user='',body=body )

@app.route('/')
def index():
    body=html   
    return template('./templates/master_page.tpl', user='',body=body )


@app.route('/<base>/<filepath:path>')
def server_static(base,filepath):
    return static_file(filepath, root=base)    

run(app, host='127.0.0.1', port=8989,reloader=True)
